package com.appmeta.test;

import android.app.Application;

import com.appmeta.gaid.GaidUtil;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        GaidUtil.init(this);
    }
}
