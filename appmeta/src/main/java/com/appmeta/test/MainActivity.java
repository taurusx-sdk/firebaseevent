package com.appmeta.test;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.appmeta.R;
import com.appmeta.gaid.GaidUtil;
import com.appmeta.webview.AppMetaActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_appmeta).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppMetaActivity.start(MainActivity.this,
                        false,
                        GaidUtil.getAdvertisingId(),
                        "994efd28aa87426daa880f6fd3203f16",
                        "K573S2");
            }
        });

        findViewById(R.id.button_appmeta_preload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppMetaActivity.start(MainActivity.this,
                        true,
                        GaidUtil.getAdvertisingId(),
                        "994efd28aa87426daa880f6fd3203f16",
                        "K573S2");
            }
        });
    }
}