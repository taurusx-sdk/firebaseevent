package com.appmeta.webview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;

import com.appmeta.R;
import com.appmeta.redirect.LogUtil;
import com.appmeta.redirect.RedirectHelper;

public class AppMetaActivity extends AppCompatActivity {

    private static final String TAG = "AppMetaActivity";

    private static final String EXTRA_PRE_LOAD = "pre_load";
    private static final String EXTRA_URL = "url";

    public static void start(Context context, boolean preload, String gaid, String appId, String pid) {
        String url = "https://api.appmeta.store/landings/h5/frame/apm3281?gaid=%s&app_id=%s&pid=%s";
        url = String.format(url, gaid, appId, pid);

        Intent intent = new Intent(context, AppMetaActivity.class);
        intent.putExtra(EXTRA_PRE_LOAD, preload);
        intent.putExtra(EXTRA_URL, url);
        if (!(context instanceof Activity)) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    private boolean mPreLoad;
    private TextView mShowTextView;
    private FrameLayout mWebViewLayout;
    private WebView mWebView;
    private ContentLoadingProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appmeta);

        mPreLoad = getIntent().getBooleanExtra(EXTRA_PRE_LOAD, false);

        mWebViewLayout = findViewById(R.id.layout_webview);
        if (mPreLoad) {
            mWebView = new WebView(this);
            mShowTextView = findViewById(R.id.textView_show);
            mShowTextView.setVisibility(View.VISIBLE);
            mShowTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mPreLoad) {
                        if (mWebView.getParent() == null) {
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT);
                            mWebViewLayout.addView(mWebView, params);
                        }
                    }
                }
            });
        } else {
            mWebView = findViewById(R.id.webView);
            mWebView.setVisibility(View.VISIBLE);
        }
        mProgressBar = findViewById(R.id.progressBar);

        initWebView();

        String url = getIntent().getStringExtra(EXTRA_URL);
        LogUtil.d(TAG, "url: " + url);
        loadUrl(url);
    }


    @SuppressWarnings("deprecation")
    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);

        WebSettings webSettings = mWebView.getSettings();
        // webSettings.setSupportZoom(true);
        // if set this, can't action onTouch()
        // webSettings.setBuiltInZoomControls(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        mWebView.requestFocus();
        if (Build.VERSION.SDK_INT > 8) {
            webSettings.setPluginState(WebSettings.PluginState.ON);
        }
        webSettings.setAllowFileAccess(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                LogUtil.d(TAG, "onPageStarted: " + url);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                LogUtil.d(TAG, "onPageFinished: " + url);
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                LogUtil.d(TAG, "onLoadResource: " + url);
                super.onLoadResource(view, url);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                LogUtil.d(TAG, "onReceivedError: " + description + ", url: " + failingUrl);
                super.onReceivedError(view, errorCode, description, failingUrl);
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                LogUtil.d(TAG, "shouldOverrideUrlLoading: " + url);
                if (url.startsWith("https://api.appmeta.store")) {
                    return super.shouldOverrideUrlLoading(view, url);
                } else {
                    RedirectHelper.getInstance().jumpToGooglePlay(AppMetaActivity.this, url);
                    return true;
                }
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
    }

    private void loadUrl(String url) {
        mWebView.loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyWebView();
    }

    @Override
    public void onBackPressed() {
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private void destroyWebView() {
        if (null != mWebView) {
            mWebView.getSettings().setBuiltInZoomControls(true);
            mWebView.setVisibility(View.GONE);
            long delayTime = ViewConfiguration.getZoomControlsTimeout();
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    mWebView.destroy();
                    mWebView = null;
                }
            }, delayTime);
        }
    }
}