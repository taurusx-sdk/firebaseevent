package com.appmeta.redirect;

public interface RedirectResult {

    String TYPE_SUCCESS_MARKET = "Success: Market Url";
    String TYPE_SUCCESS_GOOGLE_PLAY = "Success: Google Play Url";

    String TYPE_ERROR_JUMP_MARKET_FAIL = "Error: Jump Market Fail";
    String TYPE_ERROR_JUMP_GOOGLE_PLAY_FAIL = "Error: Jump Google Play Fail";

    // 不是 Http 和 Https
    String TYPE_ERROR_INVALID_URL = "Error: Invalid Url";

    String TYPE_ERROR_URL_CONNECTION_NULL = "Error: URLConnection Null";
    String TYPE_ERROR_REDIRECT_EXCEPTION = "Error: Redirect Exception";
    String TYPE_ERROR_REDIRECT_LOCATION_EMPTY = "Error: Redirect Location Empty";
    String TYPE_ERROR_END_WITH_NO_REDIRECT = "Error: End With No Redirect";

    void onFinish(boolean success, String type, String message);
}