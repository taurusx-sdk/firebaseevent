package com.appmeta.redirect;

import android.util.Log;

import com.appmeta.BuildConfig;

public final class LogUtil {

    private final static String LOG_TAG = "AppMetaActivity";
    private final static String TAG_PREFIX = LOG_TAG;

    private LogUtil() {
    }

    public static boolean isEnable() {
        return BuildConfig.DEBUG || Log.isLoggable(LOG_TAG, Log.VERBOSE);
    }

    public static void v(String tag, String message) {
        if (isEnable()) {
            Log.v(TAG_PREFIX + tag, message, null);
        }
    }

    public static void d(String tag, String message) {
        if (isEnable()) {
            Log.d(TAG_PREFIX + tag, message, null);
        }
    }

    public static void i(String tag, String message) {
        if (isEnable()) {
            Log.i(TAG_PREFIX + tag, message);
        }
    }

    public static void w(String tag, String message) {
        if (isEnable()) {
            Log.w(TAG_PREFIX + tag, message);
        }
    }

    public static void e(String tag, String message) {
        e(tag, message, null);
    }

    public static void e(String tag, String message, Throwable tr) {
        if (isEnable()) {
            Log.e(TAG_PREFIX + tag, message, tr);
        }
    }
}
