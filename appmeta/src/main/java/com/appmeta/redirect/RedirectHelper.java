package com.appmeta.redirect;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import java.net.HttpURLConnection;
import java.net.URL;

public class RedirectHelper {

    private static RedirectHelper mInstance;

    private static final String TAG = "RedirectHelper";

    private static final String GOOGLE_PLAY_PREFIX = "https://play.google.com/store/apps/";
    private static final String DEFAULT_MARKET_PREFIX = "market://";
    private static final String URL_HTTP = "http://";
    private static final String URL_HTTPS = "https://";

    public static final String PACKAGE_NAME_GOOGLE_PLAY = "com.android.vending";

    private ProgressDialog mProgressDialog;

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private RedirectHelper() {
    }

    public static RedirectHelper getInstance() {
        if (mInstance == null) {
            synchronized (RedirectHelper.class) {
                if (mInstance == null) {
                    mInstance = new RedirectHelper();
                }
            }
        }
        return mInstance;
    }

    // 是否重定向
    public boolean redirectStatus() {
        return true;
    }

    // 重定向失败是否调整浏览器
    private boolean jumpBrowserAfterRedirectFail() {
        return true;
    }

    public void jumpToGooglePlay(Context context, String url) {
        if (!redirectStatus()) {
            jumpToBrowser(context, url);
            return;
        }

        if (context instanceof Activity) {
            showProgressDialog(context);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean jumpBrowserAfterRedirectFail = jumpBrowserAfterRedirectFail();
                processUrl(context, url, new RedirectResult() {
                    @Override
                    public void onFinish(boolean success, String type, String message) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                dismissProgressDialog();
                                if (success) {
                                    LogUtil.d(TAG, "processUrl success: " + type);
                                } else {
                                    LogUtil.e(TAG, "processUrl fail: " + type);
                                    if (jumpBrowserAfterRedirectFail) {
                                        jumpToBrowser(context, url);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        }).start();
    }

    public void processUrl(Context context, String requestUrl, RedirectResult result) {
        LogUtil.d(TAG, "processUrl: " + requestUrl);
        if (requestUrl.toLowerCase().startsWith(DEFAULT_MARKET_PREFIX)) {
            try {
                jumpToGooglePlayInner(context, requestUrl);
                result.onFinish(true, RedirectResult.TYPE_SUCCESS_MARKET, requestUrl);
            } catch (Exception | Error e) {
                e.printStackTrace();
                LogUtil.e(TAG, "JumpToGooglePlay Exception");
                result.onFinish(false, RedirectResult.TYPE_ERROR_JUMP_MARKET_FAIL, requestUrl);
            }
        } else if (requestUrl.startsWith(GOOGLE_PLAY_PREFIX)) {
            try {
                jumpToGooglePlayInner(context, requestUrl);
                result.onFinish(true, RedirectResult.TYPE_SUCCESS_GOOGLE_PLAY, requestUrl);
            } catch (Exception | Error e) {
                LogUtil.e(TAG, "JumpToGooglePlay Exception");
                result.onFinish(false, RedirectResult.TYPE_ERROR_JUMP_GOOGLE_PLAY_FAIL, requestUrl);
            }
        } else if (!requestUrl.startsWith(URL_HTTP) && !requestUrl.startsWith(URL_HTTPS)) {
            result.onFinish(false, RedirectResult.TYPE_ERROR_INVALID_URL, requestUrl);
        } else {
            try {
                HttpURLConnection urlConnection = generateConnect(requestUrl);
                if (urlConnection == null) {
                    result.onFinish(false, RedirectResult.TYPE_ERROR_URL_CONNECTION_NULL, "");
                } else {
                    int statusCode = urlConnection.getResponseCode();
                    LogUtil.d(TAG, "Status code: " + statusCode);
                    if (needRedirect(statusCode)) {
                        String location = urlConnection.getHeaderField("Location");
                        LogUtil.d(TAG, "Redirect url: " + location);
                        if (!TextUtils.isEmpty(location)) {
                            processUrl(context, location, result);
                        } else {
                            result.onFinish(false, RedirectResult.TYPE_ERROR_REDIRECT_LOCATION_EMPTY, requestUrl);
                        }
                    } else {
                        result.onFinish(false, RedirectResult.TYPE_ERROR_END_WITH_NO_REDIRECT, requestUrl);
                    }
                }
            } catch (Exception | Error e) {
                e.printStackTrace();
                result.onFinish(false, RedirectResult.TYPE_ERROR_REDIRECT_EXCEPTION,
                        e.getClass().getSimpleName() + ", " + requestUrl);
            }
        }
    }

    private boolean needRedirect(int statusCode) {
        return statusCode == 301 || statusCode == 302 || statusCode == 303
                || statusCode == 307 || statusCode == 308;
    }

    private HttpURLConnection generateConnect(String url) {
        try {
            URL redirectUrl = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) redirectUrl.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(15 * 1000);
            urlConnection.setReadTimeout(15 * 1000);
            urlConnection.connect();
            return urlConnection;
        } catch (Exception | Error e) {
            e.printStackTrace();
        }
        return null;
    }

    private void jumpToGooglePlayInner(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        intent.setPackage(PACKAGE_NAME_GOOGLE_PLAY);
        context.startActivity(intent);
    }

    @SuppressLint("QueryPermissionsNeeded")
    private void jumpToBrowser(Context context, String url) {
        if (TextUtils.isEmpty(url)) {
            LogUtil.d(TAG, "Ad's click url is empty.");
            return;
        }
        try {
            Intent intent2 = new Intent(Intent.ACTION_VIEW);
            intent2.setData(Uri.parse(url));
            if (intent2.resolveActivity(context.getPackageManager()) != null) {
                LogUtil.d(TAG, "jumpToBrowser");
                context.startActivity(intent2);
            } else {
                LogUtil.d(TAG, "The current device does not have a browser!");
            }
        } catch (Exception | Error e) {
            LogUtil.d(TAG, "jumpToBrowser Exception");
        }
    }

    private void showProgressDialog(Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.show();
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}