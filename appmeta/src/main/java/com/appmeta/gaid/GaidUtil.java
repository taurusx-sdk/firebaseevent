package com.appmeta.gaid;

import android.content.Context;
import android.text.TextUtils;

public class GaidUtil {

    public static final String DEFAULT_VALUE = "00000000-0000-0000-0000-000000000000";

    private static String sAdvertisingId;

    public static void init(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                requestAdvertisingId(context);
            }
        }).start();
    }

    public static String getAdvertisingId() {
        return DEFAULT_VALUE.equals(sAdvertisingId) ? "" : sAdvertisingId;
    }

    private static String requestAdvertisingId(Context context) {
        if (context == null) {
            return DEFAULT_VALUE.equals(sAdvertisingId) ? "" : sAdvertisingId;
        }

        if (TextUtils.isEmpty(sAdvertisingId)) {
            try {
                sAdvertisingId = AdvertisingIdClient.getAdvertisingIdInfo(context).getId();
            } catch (Exception | Error e) {
                sAdvertisingId = DEFAULT_VALUE;
            }
            return sAdvertisingId;
        }

        return DEFAULT_VALUE.equals(sAdvertisingId) ? "" : sAdvertisingId;
    }
}