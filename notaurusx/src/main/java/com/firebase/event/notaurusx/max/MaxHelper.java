package com.firebase.event.notaurusx.max;

import android.util.Log;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.firebase.event.notaurusx.common.ADs;
import com.firebase.event.notaurusx.common.Network;
import com.firebase.event.notaurusx.common.SecondaryLineItem;

public class MaxHelper {

    public static SecondaryLineItem generateSecondaryLineItem(String TAG, MaxAd ad) {
        Network network = fromNetworkName(ad.getNetworkName());
        // 如果分层是 AppLovin，使用 Max 的 AdUnitId，否则使用 Network 的 PlacementId
        String adUnitId = network == Network.APPLOVIN || network == Network.APPLOVIN_EXCHANGE
                ? ad.getAdUnitId()
                : ad.getNetworkPlacement();
        double eCPM = ad.getRevenue() > 0 ? ad.getRevenue() * 1000 : 0;
        SecondaryLineItem secondaryLineItem = SecondaryLineItem.newBuilder()
                .setNetwork(network)
                .setAdUnitId(adUnitId)
                .seteCPM(eCPM)
                .build();
        Log.d(TAG, "generateSecondaryLineItem: " + secondaryLineItem);

        return secondaryLineItem;
    }

    public static Network fromNetworkName(String name) {
        if (name.toLowerCase().contains("facebook")) {
            return Network.FACEBOOK;
        }
        if (name.toLowerCase().contains("verve")) {
            return Network.VERVE;
        }
        switch (name) {
            case "AdColony":
                return Network.ADCOLONY;
            case "Google AdMob":
                return Network.ADMOB;
            case "AppLovin":
                return Network.APPLOVIN;
            case "Chartboost":
                return Network.CHARTBOOST;
            case "Fyber":
                return Network.FYBER;
            case "Google Ad Manager":
                return Network.DFP;
            case "InMobi":
                return Network.INMOBI;
            case "IronSource":
                return Network.IRON_SOURCE;
            case "Maio":
                return Network.MAIO;
            case "Mintegral":
                return Network.MINTEGRAL;
            case "MoPub":
                return Network.MOPUB;
            case "Nend":
                return Network.NEND;
            case "Pangle":
                return Network.PANGLE;
            case "Tapjoy":
                return Network.TAPJOY;
            case "Unity Ads":
                return Network.UNITY;
            case "Vungle":
                return Network.VUNGLE;
            case "APPLOVIN_EXCHANGE":
                return Network.APPLOVIN_EXCHANGE;
            case "AdFly":
                return Network.ADFLY;
            default:
                return Network.UNKNOWN;
        }
    }

    public static String getAdType(MaxAd maxAd) {
        MaxAdFormat maxAdFormat = maxAd.getFormat();
        String label = maxAdFormat.getLabel();
        if (MaxAdFormat.BANNER.getLabel().equals(label)
                || MaxAdFormat.MREC.getLabel().equals(label)
                || MaxAdFormat.LEADER.getLabel().equals(label)) {
            return ADs.TYPE_BANNER;
        }
        if (label.equals(MaxAdFormat.INTERSTITIAL.getLabel())) {
            return ADs.TYPE_INTERSTITIAL;
        }
        if (MaxAdFormat.NATIVE.getLabel().equals(label)) {
            return ADs.TYPE_NATIVE;
        }
        if (MaxAdFormat.REWARDED.getLabel().equals(label)
                || MaxAdFormat.REWARDED_INTERSTITIAL.getLabel().equals(label)) {
            return ADs.TYPE_REWARD;
        }
        return ADs.TYPE_UNKNOWN;
    }
}
