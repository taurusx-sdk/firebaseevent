package com.firebase.event.notaurusx.common;

public enum Network {
    UNKNOWN(0, "unknown"),
    ADCOLONY(1, "adcolony"),
    ADMOB(2, "admob"),
    APPLOVIN(3, "applovin"),
    CHARTBOOST(4, "chartboost"),
    FACEBOOK(5, "facebook"),
    IRON_SOURCE(6, "ironsource"),
    MOPUB(7, "mopub"),
    UNITY(8, "unity"),
    MARKETPLACE(9, "marketplace"),
    FYBER(10, "fyber"),
    INMOBI(11, "inmobi"),
    VUNGLE(12, "vungle"),
    DFP(13, "google_ad_manager"),
    CREATIVE(14, "creative"),
    DAP(15, "dap"),
    BAIDU(16, "baidu"),
    DISPLAYIO(17, "displayio"),
    TOUTIAO(18, "toutiao"),
    GDT(19, "gdt"),
    AMAZON(20, "amazon"),
    FLURRY(21, "flurry"),
    TAPJOY(22, "tapjoy"),
    _360(23, "360"),
    XIAOMI(24, "xiaomi"),
    _4399(25, "4399"),
    OPPO(26, "oppo"),
    VIVO(27, "vivo"),
    MINTEGRAL(28, "mintegral"),
    NEND(29, "nend"),
    ADGENERATION(30, "adgeneration"),
    MAIO(31, "maio"),
    ALIGAMES(32, "aligames"),
    CRITEO(33, "criteo"),
    ZHONGHUI_ADS(34, "zhonghui_ads"),
    TMS(35, "tms"),
    FIVE(36, "five"),
    KUAISHOU(37, "kuaishou"),
    IMOBILE(38, "imobile"),
    PANGLE(39, "pangle"),
    SIGMOB(40, "sigmob"),
    PREBID(41, "prebid"),
    OUPENG(42, "oupeng"),
    APPNEXUS(43, "appnexus"),
    IFLY(44, "ifly"),
    TUIA(45, "tuia"),
    YOUDAO(46, "youdao"),
    APPLOVIN_MAX(47, "applovin_max"),
    MOBRAIN(48, "mobrain"),
    ALIBC(49, "alibc"),
    QTT(50, "qtt"),
    LINKAI(51, "linkai"),
    JAD(52, "jad"),
    YKY(53, "yky"),
    TOPON(54, "topon"),
    MEISHU(55, "meishu"),
    TRADPLUS(56, "tradplus"),
    APPLOVIN_EXCHANGE(57, "applovin_exchange"),
    SAN(58, "san"),
    VERVE(59, "verve"),
    MINT(60, "mint"),
    ADFLY(61, "adfly");

    private int mId;
    private String mName;

    Network(int id, String name) {
        mId = id;
        mName = name;
    }

    public static Network fromId(int id) {
        Network[] networks = Network.values();
        for (Network network : networks) {
            if (network.mId == id) {
                return network;
            }
        }
        Network network = UNKNOWN;
        network.mId = id;
        return network;
    }

    public int getNetworkId() {
        return mId;
    }

    public String getNetworkName() {
        return mName;
    }

    @Override
    public String toString() {
        return "Network id is " + mId + ", name is " + mName;
    }
}
