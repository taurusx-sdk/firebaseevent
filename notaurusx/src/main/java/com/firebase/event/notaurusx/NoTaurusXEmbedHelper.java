package com.firebase.event.notaurusx;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.applovin.mediation.MaxAd;
import com.firebase.event.notaurusx.admob.AdMobHelper;
import com.firebase.event.notaurusx.common.ADs;
import com.firebase.event.notaurusx.common.SecondaryLineItem;
import com.firebase.event.notaurusx.max.MaxHelper;
import com.firebase.event.notaurusx.mopub.MoPubHelper;
import com.firebase.event.notaurusx.tradplus.TradPlusHelper;
import com.google.android.gms.ads.ResponseInfo;
import com.mopub.network.ImpressionData;
import com.mopub.network.ImpressionListener;
import com.mopub.network.ImpressionsEmitter;
import com.tradplus.ads.base.bean.TPAdInfo;

import java.util.HashMap;
import java.util.Map;

import we.studio.embed.EmbedSDK;

/**
 * 不使用 TaurusX，直接使用其他聚合平台时，使用此类提供的方法来上报 w_ad_imp 事件到 Embed。
 * 支持的聚合平台有：MoPub、Max、AdMob。
 */
public class NoTaurusXEmbedHelper {

    // 使用 MoPub SDK 加载广告，发送 w_ad_imp 事件到 Embed
    // 仅在 App 启动时调用一次
    public static void send_w_ad_impForMoPub(Context context) {
        if (context == null) {
            return;
        }

        ImpressionsEmitter.addListener(new ImpressionListener() {
            @Override
            public void onImpression(@NonNull String adUnitId,
                                     @Nullable ImpressionData impressionData) {
                if (impressionData == null) {
                    return;
                }
                String sdkName = ADs.AD_SDK_MOPUB; // MoPub
                String type = MoPubHelper.getAdTypeFromAdUnitFormat(impressionData.getAdUnitFormat());

                Bundle bundle = new Bundle();
                bundle.putString("sdk_name", ADs.transSdkName(sdkName));
                if (!TextUtils.isEmpty(adUnitId)) {
                    bundle.putString("pid", adUnitId);
                }
                if (!TextUtils.isEmpty(type)) {
                    bundle.putString("type", ADs.transType(type));
                }
                try {
                    SecondaryLineItem secondaryLineItem =
                            MoPubHelper.generateSecondaryLineItem("NoTaurusXHelper", impressionData);
                    if (secondaryLineItem != null) {
                        String secSdkName = String.valueOf(secondaryLineItem.getNetwork().getNetworkId());
                        bundle.putString("m_sdk_name", ADs.transSdkName(secSdkName));
                        bundle.putString("m_pid", secondaryLineItem.getAdUnitId());
                        bundle.putString("m_ecpm", String.valueOf(secondaryLineItem.geteCPM()));
                    }
                } catch (Exception | Error e) {
                    e.printStackTrace();
                }

                // 发送到 Embed
                Map<String, String> eventValue = new HashMap<>();
                for (String key : bundle.keySet()) {
                    eventValue.put(key, bundle.getString(key));
                }
                EmbedSDK.reportCustomEvent(context, "w_ad_imp", eventValue);
            }
        });
    }

    // 使用 Max SDK 加载广告，发送 w_ad_imp 事件到 Embed
    // 在所有 Max 广告的 onAdRevenuePaid() 回调中调用 send_w_ad_impForMax()
    // 参考：https://dash.applovin.com/documentation/mediation/android/getting-started/advanced-settings
    public static void send_w_ad_impForMax(Context context, MaxAd maxAd) {
        if (context == null || maxAd == null) {
            return;
        }

        String sdkName = ADs.AD_SDK_APPLOVIN_MAX; // Max
        String type = MaxHelper.getAdType(maxAd);

        Bundle bundle = new Bundle();
        bundle.putString("sdk_name", ADs.transSdkName(sdkName));
        if (!TextUtils.isEmpty(maxAd.getAdUnitId())) {
            bundle.putString("pid", maxAd.getAdUnitId());
        }
        if (!TextUtils.isEmpty(type)) {
            bundle.putString("type", ADs.transType(type));
        }
        // 如果同一个广告位用在多个场景中，可以通过 item_id 区分
        // bundle.putString("item_id", "you_scene_id");
        try {
            SecondaryLineItem secondaryLineItem =
                    MaxHelper.generateSecondaryLineItem("NoTaurusXHelper", maxAd);
            if (secondaryLineItem != null) {
                String secSdkName = String.valueOf(secondaryLineItem.getNetwork().getNetworkId());
                bundle.putString("m_sdk_name", ADs.transSdkName(secSdkName));
                bundle.putString("m_pid", secondaryLineItem.getAdUnitId());
                bundle.putString("m_ecpm", String.valueOf(secondaryLineItem.geteCPM()));
            }
        } catch (Exception | Error e) {
            e.printStackTrace();
        }

        // 发送到 Embed
        Map<String, String> eventValue = new HashMap<>();
        for (String key : bundle.keySet()) {
            eventValue.put(key, bundle.getString(key));
        }
        EmbedSDK.reportCustomEvent(context, "w_ad_imp", eventValue);
    }

    // 使用 AdMob SDK 加载广告，发送 w_ad_imp 事件到 Embed
    // 在 AdMob 广告的 onAdImpression() 回调中调用 send_w_ad_impForAdMob()
    // 参数通过调用 AdMob InterstitialAd/RewardedAd 的 getAdUnitId()、getResponseInfo() 获得
    public static void send_w_ad_impForAdMob(Context context,
                                             String adUnitId,
                                             String adType,
                                             ResponseInfo responseInfo) {
        if (context == null || responseInfo == null) {
            return;
        }

        String sdkName = ADs.AD_SDK_ADMOB; // AdMob

        Bundle bundle = new Bundle();
        bundle.putString("sdk_name", ADs.transSdkName(sdkName));
        if (!TextUtils.isEmpty(adUnitId)) {
            bundle.putString("pid", adUnitId);
        }
        bundle.putString("type", ADs.transType(adType));
        try {
            SecondaryLineItem secondaryLineItem =
                    AdMobHelper.generateSecondaryLineItem("NoTaurusXHelper", adUnitId, responseInfo);
            if (secondaryLineItem != null) {
                String secSdkName = String.valueOf(secondaryLineItem.getNetwork().getNetworkId());
                bundle.putString("m_sdk_name", ADs.transSdkName(secSdkName));
                bundle.putString("m_pid", secondaryLineItem.getAdUnitId());
                bundle.putString("m_ecpm", String.valueOf(secondaryLineItem.geteCPM()));
            }
        } catch (Exception | Error e) {
            e.printStackTrace();
        }

        // 发送到 Embed
        Map<String, String> eventValue = new HashMap<>();
        for (String key : bundle.keySet()) {
            eventValue.put(key, bundle.getString(key));
        }
        EmbedSDK.reportCustomEvent(context, "w_ad_imp", eventValue);
    }

    // 使用 TradPlus SDK 加载广告，发送 w_ad_imp 事件到 Embed
    // 在 TradPlus 广告的 onAdImpression() 回调中调用 send_w_ad_impForTradPlus()
    // 参数为 onAdImpression() 回调的 TPAdInfo 对象
    public static void send_w_ad_impForTradPlus(Context context,
                                                TPAdInfo tpAdInfo,
                                                String adType) {
        if (context == null || tpAdInfo == null) {
            return;
        }

        String sdkName = ADs.AD_SDK_TradPlus; // TradPlus
        Bundle bundle = new Bundle();
        bundle.putString("sdk_name", ADs.transSdkName(sdkName));
        if (!TextUtils.isEmpty(tpAdInfo.tpAdUnitId)) {
            bundle.putString("pid", tpAdInfo.tpAdUnitId);
        }
        bundle.putString("type", ADs.transType(adType));
        try {
            SecondaryLineItem secondaryLineItem =
                    TradPlusHelper.generateSecondaryLineItem("NoTaurusXHelper", tpAdInfo);
            if (secondaryLineItem != null) {
                String secSdkName = String.valueOf(secondaryLineItem.getNetwork().getNetworkId());
                bundle.putString("m_sdk_name", ADs.transSdkName(secSdkName));
                bundle.putString("m_pid", secondaryLineItem.getAdUnitId());
                bundle.putString("m_ecpm", String.valueOf(secondaryLineItem.geteCPM()));
            }
        } catch (Exception | Error e) {
            e.printStackTrace();
        }

        // 发送到 Embed
        Map<String, String> eventValue = new HashMap<>();
        for (String key : bundle.keySet()) {
            eventValue.put(key, bundle.getString(key));
        }
        EmbedSDK.reportCustomEvent(context, "w_ad_imp", eventValue);
    }
}