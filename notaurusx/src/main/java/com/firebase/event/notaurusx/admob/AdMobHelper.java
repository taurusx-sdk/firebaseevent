package com.firebase.event.notaurusx.admob;

import android.os.Bundle;
import android.util.Log;

import com.firebase.event.notaurusx.common.Network;
import com.firebase.event.notaurusx.common.SecondaryLineItem;
import com.google.android.gms.ads.AdapterResponseInfo;
import com.google.android.gms.ads.ResponseInfo;

import java.util.ArrayList;
import java.util.Arrays;

public class AdMobHelper {

    public static void logResponseInfo(String TAG, ResponseInfo responseInfo) {
        if (responseInfo != null) {
            String loadedClassName = responseInfo.getMediationAdapterClassName();
            Log.d(TAG, "logResponseInfo, loaded ClassName: " + loadedClassName
                    + ", ResponseId: " + responseInfo.getResponseId());
            if (loadedClassName != null) {
                for (AdapterResponseInfo info : responseInfo.getAdapterResponses()) {
                    Log.d(TAG, "logResponseInfo, AdapterResponseInfo, ClassName: " + info.getAdapterClassName()
                            + ", AdError: " + info.getAdError()
                            + ", Credentials: " + info.getCredentials()
                            + ", LatencyMillis: " + info.getLatencyMillis() + "ms");
                    for (String key : info.getCredentials().keySet()) {
                        Log.d(TAG, "logResponseInfo, Credentials bundle : " + key
                                + ": " + info.getCredentials().get(key));
                    }

                    NetworkInfo networkInfo = generateNetworkInfo(
                            info.getAdapterClassName(),
                            "",
                            info.getCredentials());
                    Log.d(TAG, "logResponseInfo, Network: " + networkInfo.mNetwork
                            + ", adUnitId: " + networkInfo.mAdUnitId);
                    Log.d(TAG, "==========");
                }
            }
        } else {
            Log.e(TAG, "logResponseInfo, responseInfo is null");
        }
    }

    // 是否接入 AdMob 聚合
    private static boolean isAdMobMediation(ResponseInfo responseInfo) {
        if (responseInfo == null) {
            return false;
        }
        // 如果只有一层且是 AdMob，认为不是通过 AdMob 聚合
        if (responseInfo.getAdapterResponses().size() == 1
                && responseInfo.getAdapterResponses().get(0).getAdapterClassName().contains("AdMob")) {
            return false;
        }
        return responseInfo.getAdapterResponses().size() > 1;
    }

    public static SecondaryLineItem generateSecondaryLineItem(String TAG,
                                                              String adUnitId,
                                                              ResponseInfo responseInfo) {
        if (!isAdMobMediation(responseInfo)) {
            Log.d(TAG, "not AdMob Mediation, return null SecondaryLineItem");
            return null;
        }

        NetworkInfo networkInfo = new NetworkInfo(Network.UNKNOWN, "");
        logResponseInfo(TAG, responseInfo);

        String loadedClassName = responseInfo.getMediationAdapterClassName();
        if (loadedClassName != null) {
            for (AdapterResponseInfo info : responseInfo.getAdapterResponses()) {
                if (loadedClassName.equals(info.getAdapterClassName())) {
                    networkInfo = generateNetworkInfo(loadedClassName, adUnitId, info.getCredentials());
                    break;
                }
            }
        }

        SecondaryLineItem secondaryLineItem = SecondaryLineItem.newBuilder()
                .setNetwork(networkInfo.mNetwork)
                .setAdUnitId(networkInfo.mAdUnitId != null ? networkInfo.mAdUnitId : "")
                .build();
        Log.d(TAG, "generateSecondaryLineItem: " + secondaryLineItem);
        return secondaryLineItem;
    }

    private static class NetworkInfo {
        public Network mNetwork;
        public String mAdUnitId;

        private NetworkInfo(Network network, String adUnitId) {
            mNetwork = network;
            mAdUnitId = adUnitId;
        }
    }

    private static NetworkInfo generateNetworkInfo(String className, String adUnitId, Bundle credentials) {
        if (className.contains("AdColony")) {
            String requestedZones = credentials.getString("zone_ids");
            if (requestedZones == null) {
                requestedZones = credentials.getString("zone_id");
            }
            ArrayList<String> newZoneList = null;
            if (requestedZones != null) {
                newZoneList = new ArrayList<>(Arrays.asList(requestedZones.split(";")));
            }
            String requestedZone = "";
            if (newZoneList != null && !newZoneList.isEmpty()) {
                requestedZone = newZoneList.get(0);
            }
            return new NetworkInfo(Network.ADCOLONY, requestedZone);
        } else if (className.contains("AdMob")) {
            return new NetworkInfo(Network.ADMOB, adUnitId);
        } else if (className.contains("Applovin")) {
            return new NetworkInfo(Network.APPLOVIN, credentials.getString("zone_id"));
        } else if (className.contains("Chartboost")) {
            return new NetworkInfo(Network.CHARTBOOST, credentials.getString("adLocation"));
        } else if (className.contains("Facebook")) {
            String placementId = credentials.getString("placement_id");
            if (placementId == null) {
                // Fall back to checking the non-open bidding key.
                placementId = credentials.getString("pubid");
            }
            return new NetworkInfo(Network.FACEBOOK, placementId);
        } else if (className.contains("IronSource")) {
            return new NetworkInfo(Network.IRON_SOURCE, credentials.getString("instanceId", "0"));
        } else if (className.contains("MoPub")) {
            return new NetworkInfo(Network.MOPUB, credentials.getString("adUnitId"));
        } else if (className.contains("Unity")) {
            return new NetworkInfo(Network.UNITY, credentials.getString("zoneId"));
        } else if (className.contains("Fyber")) {
            return new NetworkInfo(Network.FYBER, credentials.getString("spotId"));
        } else if (className.contains("InMobi")) {
            return new NetworkInfo(Network.INMOBI, credentials.getString("placementid"));
        } else if (className.contains("Vungle")) {
            return new NetworkInfo(Network.VUNGLE, credentials.getString("placementID"));
        } else if (className.contains("Tapjoy")) {
            return new NetworkInfo(Network.TAPJOY, credentials.getString("placementName"));
        } else if (className.contains("Nend")) {
            return new NetworkInfo(Network.NEND, credentials.getString("spotId"));
        } else if (className.toLowerCase().contains("maio")) {
            return new NetworkInfo(Network.MAIO, credentials.getString("zoneId"));
        } else if (className.contains("IMobile")) {
            return new NetworkInfo(Network.IMOBILE, credentials.getString("asid"));
        } else {
            return new NetworkInfo(Network.UNKNOWN, "");
        }
    }
}
