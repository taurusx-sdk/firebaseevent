package com.firebase.event.notaurusx.tradplus;

import android.util.Log;

import com.firebase.event.notaurusx.common.Network;
import com.firebase.event.notaurusx.common.SecondaryLineItem;
import com.tradplus.ads.base.bean.TPAdInfo;
import com.tradplus.ads.network.response.ConfigResponse;

public class TradPlusHelper {

    private static final String TAG = "TradPlusHelper";

    public static SecondaryLineItem generateSecondaryLineItem(String tag, TPAdInfo tpAdInfo) {
        float eCPM = 0;
        try {
            eCPM = Float.parseFloat(tpAdInfo.ecpm); // 美金
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        Network network = fromAdNetworkId(tpAdInfo.adNetworkId);
        String adUnitId = tpAdInfo.adSourceId;
        // 对于 Mintegral，需要获取 AdUnit Id
        if (network == Network.MINTEGRAL) {
            ConfigResponse.WaterfallBean.ConfigBean configBean = tpAdInfo.configBean;
            if (configBean != null) {
                adUnitId = configBean.getUnitId();
            }
        }
        SecondaryLineItem secondaryLineItem = SecondaryLineItem.newBuilder()
                .setNetwork(network)
                .setAdUnitId(adUnitId)
                .seteCPM(eCPM)
                .build();
        Log.d(tag, "generateSecondaryLineItem: " + secondaryLineItem);
        return secondaryLineItem;
    }

    private static Network fromAdNetworkId(String networkId) {
        switch (networkId) {
            case "1":
                return Network.FACEBOOK;
            case "2":
                return Network.ADMOB;
            case "3":
                return Network.MOPUB;
            case "4":
                return Network.ADCOLONY;
            case "5":
                return Network.UNITY;
            case "6":
                return Network.TAPJOY;
            case "7":
                return Network.VUNGLE;
            case "9":
                return Network.APPLOVIN;
            case "10":
                return Network.IRON_SOURCE;
            case "15":
                return Network.CHARTBOOST;
            case "16":
                return Network.GDT;
            case "17":
                return Network.TOUTIAO;
            case "18":
                return Network.MINTEGRAL;
            case "19":
                return Network.PANGLE;
            case "20":
                return Network.KUAISHOU;
            case "21":
                return Network.SIGMOB;
            case "23":
                return Network.INMOBI;
            case "24":
                return Network.FYBER;
            case "25":
                return Network.YOUDAO;
            case "31":
                return Network.MAIO;
            case "32":
                return Network.CRITEO;
            case "43":
                return Network.BAIDU;
            case "44":
                return Network.YKY;
            default:
                return Network.UNKNOWN;
        }
    }
}
