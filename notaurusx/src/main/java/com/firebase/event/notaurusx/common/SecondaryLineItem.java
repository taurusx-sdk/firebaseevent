package com.firebase.event.notaurusx.common;

import androidx.annotation.NonNull;

/**
 * 二级 LineItem 信息。
 * 为聚合类型 Network 的分层信息，比如 Mobrain、AppLovinMax、AdMob、MoPub 等。
 */
public class SecondaryLineItem {

    private Builder mBuilder;

    private SecondaryLineItem(Builder builder) {
        mBuilder = builder;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Network getNetwork() {
        return mBuilder.mNetwork;
    }

    public String getAdUnitId() {
        return mBuilder.mAdUnitId;
    }

    public double geteCPM() {
        return mBuilder.meCPM;
    }

    @NonNull
    @Override
    public String toString() {
        return "SecondaryLineItem{" +
                "mNetwork=" + getNetwork() +
                ", mAdUnitId=" + getAdUnitId() +
                ", meCPM=" + geteCPM() +
                '}';
    }

    public static class Builder {
        private Network mNetwork;
        private String mAdUnitId;
        private double meCPM;

        private Builder() {
        }

        public Builder setNetwork(Network network) {
            mNetwork = network;
            return this;
        }

        public Builder setAdUnitId(String adUnitId) {
            mAdUnitId = adUnitId;
            return this;
        }

        public Builder seteCPM(double eCPM) {
            meCPM = eCPM;
            return this;
        }

        public SecondaryLineItem build() {
            return new SecondaryLineItem(this);
        }
    }
}
