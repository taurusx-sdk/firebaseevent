package com.firebase.event.notaurusx.mopub;

import android.text.TextUtils;
import android.util.Log;

import com.firebase.event.notaurusx.common.ADs;
import com.firebase.event.notaurusx.common.Network;
import com.firebase.event.notaurusx.common.SecondaryLineItem;
import com.mopub.network.ImpressionData;

public class MoPubHelper {

    public static SecondaryLineItem generateSecondaryLineItem(String TAG, ImpressionData impressionData) {
        if (impressionData != null) {
            try {
                Log.d(TAG, "generateSecondaryLineItem, impressionData: "
                        + impressionData.getJsonRepresentation().toString(2));
            } catch (Exception | Error e) {
                e.printStackTrace();
            }
            String networkName = impressionData.getNetworkName();
            String networkPlacementId = impressionData.getNetworkPlacementId();
            Double revenue = impressionData.getPublisherRevenue();
            Log.d(TAG, "generateSecondaryLineItem, networkName: " + networkName
                    + ", networkPlacementId: " + networkPlacementId
                    + ", revenue: " + revenue);

            Network network = getNetworkFromNetworkName(networkName);
            SecondaryLineItem secondaryLineItem = SecondaryLineItem.newBuilder()
                    .setNetwork(network)
                    .setAdUnitId(network == Network.MOPUB ? impressionData.getAdUnitId() : networkPlacementId)
                    .seteCPM(revenue != null ? revenue * 1000 : 0)
                    .build();
            Log.d(TAG, "generateSecondaryLineItem: " + secondaryLineItem);
            return secondaryLineItem;
        } else {
            Log.e(TAG, "generateSecondaryLineItem, impressionData is null");
            return null;
        }
    }

    // MoPub 对 Network 的名称定义参考各 Network 源码 build.gradle 中的 project.ext.networkName：
    // https://github.com/mopub/mopub-android-mediation/blob/master/AdMob/build.gradle
    public static Network getNetworkFromNetworkName(String name) {
        if (TextUtils.isEmpty(name)) {
            return Network.MOPUB;
        }
        switch (name) {
            case "adcolony":
                return Network.ADCOLONY;
            case "admob_native":
                return Network.ADMOB;
            case "applovin_sdk":
                return Network.APPLOVIN;
            case "chartboost":
                return Network.CHARTBOOST;
            case "facebook":
                return Network.FACEBOOK;
            case "fyber":
                return Network.FYBER;
            case "inmobi_sdk":
                return Network.INMOBI;
            case "ironsource":
                return Network.IRON_SOURCE;
            case "mintegral":
                return Network.MINTEGRAL;
            case "ogury":
                return Network.UNKNOWN;
            case "pangle":
                return Network.PANGLE;
            case "snap":
                return Network.UNKNOWN;
            case "tapjoy":
                return Network.TAPJOY;
            case "unity":
                return Network.UNITY;
            case "verizon":
                return Network.UNKNOWN;
            case "vungle":
                return Network.VUNGLE;
            default:
                return Network.UNKNOWN;
        }
    }

    /**
     * - Banner
     * - Medium Rectangle
     * - Fullscreen
     * - Rewarded Ad
     * - Custom
     */
    public static String getAdTypeFromAdUnitFormat(String adUnitFormat) {
        if (adUnitFormat == null) {
            return ADs.TYPE_UNKNOWN;
        }
        switch (adUnitFormat) {
            case "Banner":
            case "Medium Rectangle":
                return ADs.TYPE_BANNER;
            case "Fullscreen":
                return ADs.TYPE_INTERSTITIAL;
            case "Rewarded Ad":
                return ADs.TYPE_REWARD;
            case "Custom":
                return ADs.TYPE_NATIVE;
            default:
                return ADs.TYPE_UNKNOWN;
        }
    }
}