package com.firebase.event.test;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdRevenueListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.firebase.event.notaurusx.NoTaurusXHelper;
import com.firebase.event.notaurusx.R;
import com.firebase.event.notaurusx.common.ADs;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdValue;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnPaidEventListener;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import com.tradplus.ads.base.bean.TPAdError;
import com.tradplus.ads.base.bean.TPAdInfo;
import com.tradplus.ads.open.TradPlusSdk;
import com.tradplus.ads.open.interstitial.InterstitialAdListener;
import com.tradplus.ads.open.interstitial.TPInterstitial;

/**
 * 测试使用 MoPub、Max、AdMob 加载&展示广告。
 */
public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initMoPub();
        findViewById(R.id.button_mopub).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testMoPub();
            }
        });

        initMax();
        findViewById(R.id.button_max).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testMax();
            }
        });

        initAdMob();
        findViewById(R.id.button_admob).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testAdMob();
            }
        });

        initTradPlus();
        findViewById(R.id.button_tradplus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testTradPlus();
            }
        });
    }

    private void initMoPub() {
        SdkConfiguration.Builder builder = new SdkConfiguration.Builder("9116ec36c196443d965c5b010e6cc053");
        builder.withLegitimateInterestAllowed(true);
        builder.withLogLevel(MoPubLog.LogLevel.DEBUG);

        MoPub.initializeSdk(this, builder.build(),
                new SdkInitializationListener() {
                    @Override
                    public void onInitializationFinished() {
                        Log.d(TAG, "onInitializationFinished");
                    }
                });
        // 不使用默认的 IN_APP，因为点击要过 3s 后才会跳转
        MoPub.setBrowserAgent(MoPub.BrowserAgent.NATIVE);
    }

    private MoPubInterstitial mMoPubInterstitial;

    private void testMoPub() {
        mMoPubInterstitial = new MoPubInterstitial(this, "9116ec36c196443d965c5b010e6cc053");
        mMoPubInterstitial.setInterstitialAdListener(
                new MoPubInterstitial.InterstitialAdListener() {
                    @Override
                    public void onInterstitialLoaded(com.mopub.mobileads.MoPubInterstitial interstitial) {
                        Log.d(TAG, "onInterstitialLoaded");
                        mMoPubInterstitial.show();
                    }

                    @Override
                    public void onInterstitialFailed(com.mopub.mobileads.MoPubInterstitial interstitial,
                                                     MoPubErrorCode errorCode) {
                        Log.e(TAG, "onInterstitialFailed");
                    }

                    @Override
                    public void onInterstitialShown(com.mopub.mobileads.MoPubInterstitial interstitial) {
                        Log.d(TAG, "onInterstitialShown");
                    }

                    @Override
                    public void onInterstitialClicked(com.mopub.mobileads.MoPubInterstitial interstitial) {
                        Log.d(TAG, "onInterstitialClicked");
                    }

                    @Override
                    public void onInterstitialDismissed(com.mopub.mobileads.MoPubInterstitial interstitial) {
                        Log.d(TAG, "onInterstitialDismissed");
                    }
                });
        mMoPubInterstitial.load();
    }

    private void initMax() {
        AppLovinSdk.getInstance(this).setMediationProvider(AppLovinMediationProvider.MAX);

        AppLovinSdkSettings settings = AppLovinSdk.getInstance(this).getSettings();
        settings.setVerboseLogging(true);

        AppLovinSdk.initializeSdk(this, new AppLovinSdk.SdkInitializationListener() {
            @Override
            public void onSdkInitialized(AppLovinSdkConfiguration config) {
                Log.d(TAG, "onSdkInitialized, countryCode: " + config.getCountryCode());
            }
        });
    }

    private MaxInterstitialAd mMaxInterstitialAd;

    private void testMax() {
        mMaxInterstitialAd = new MaxInterstitialAd("9c24dcebe57764e3", this);
        mMaxInterstitialAd.setListener(new MaxAdListener() {
            @Override
            public void onAdLoaded(MaxAd ad) {
                Log.d(TAG, "onAdLoaded");
                mMaxInterstitialAd.showAd();
            }

            @Override
            public void onAdLoadFailed(String adUnitId, MaxError error) {
                Log.d(TAG, "onAdLoadFailed");
            }

            @Override
            public void onAdDisplayed(MaxAd ad) {
                Log.d(TAG, "onAdDisplayed");
            }

            @Override
            public void onAdDisplayFailed(MaxAd ad, MaxError error) {
                Log.d(TAG, "onAdDisplayFailed");
            }

            @Override
            public void onAdClicked(MaxAd ad) {
                Log.d(TAG, "onAdClicked");
            }

            @Override
            public void onAdHidden(MaxAd ad) {
                Log.d(TAG, "onAdHidden");
            }
        });
        mMaxInterstitialAd.setRevenueListener(new MaxAdRevenueListener() {
            @Override
            public void onAdRevenuePaid(MaxAd maxAd) {
                Log.d(TAG, "onAdRevenuePaid");
                // 使用 Max SDK 加载广告，发送 w_ad_imp 事件到 Firebase
                // 在所有 Max 广告的 onAdRevenuePaid() 回调中调用 send_w_ad_impForMax()
                // 参考：https://dash.applovin.com/documentation/mediation/android/getting-started/advanced-settings
                NoTaurusXHelper.send_w_ad_impForMax(MainActivity.this, maxAd);
            }
        });
        mMaxInterstitialAd.loadAd();
    }

    private void initAdMob() {
        MobileAds.initialize(this,
                new OnInitializationCompleteListener() {
                    @Override
                    public void onInitializationComplete(@NonNull InitializationStatus initializationStatus) {
                        Log.d(TAG, "onInitializationComplete");
                    }
                });
    }

    private InterstitialAd mAdMobInterstitialAd;

    private void testAdMob() {
        AdRequest.Builder builder = new AdRequest.Builder();

        // GDPR
        Bundle extras = new Bundle();
        // 得到用户同意
        extras.putString("npa", "0");
        Log.d(TAG, "request PA");

        InterstitialAd.load(this,
                "ca-app-pub-3940256099942544/1033173712",
                builder.build(),
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        Log.d(TAG, "onAdLoaded");

                        mAdMobInterstitialAd = interstitialAd;
                        mAdMobInterstitialAd.setImmersiveMode(true);
                        mAdMobInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                            @Override
                            public void onAdShowedFullScreenContent() {
                                Log.d(TAG, "onAdShowedFullScreenContent");
                            }

                            @Override
                            public void onAdImpression() {
                                Log.d(TAG, "onAdImpression");
                                // 使用 AdMob SDK 加载广告，发送 w_ad_imp 事件到 Firebase
                                // 在 AdMob 广告的 onAdImpression() 回调中调用 send_w_ad_impForAdMob()
                                // 参数通过调用 AdMob InterstitialAd/RewardedAd 的 getAdUnitId()、getResponseInfo() 获得
                                NoTaurusXHelper.send_w_ad_impForAdMob(MainActivity.this,
                                        mAdMobInterstitialAd.getAdUnitId(),
                                        ADs.TYPE_INTERSTITIAL,
                                        mAdMobInterstitialAd.getResponseInfo());
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                Log.d(TAG, "onAdFailedToShowFullScreenContent");
                            }

                            @Override
                            public void onAdDismissedFullScreenContent() {
                                Log.d(TAG, "onAdDismissedFullScreenContent");
                            }
                        });
                        mAdMobInterstitialAd.setOnPaidEventListener(new OnPaidEventListener() {
                            @Override
                            public void onPaidEvent(@NonNull AdValue adValue) {
                                Log.d(TAG, "onPaidEvent: "
                                        + "CurrencyCode: " + adValue.getCurrencyCode()
                                        + ", PrecisionType: " + adValue.getPrecisionType()
                                        + ", ValueMicros: " + adValue.getValueMicros());
                            }
                        });
                        mAdMobInterstitialAd.show(MainActivity.this);
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        Log.e(TAG, "onAdFailedToLoad");
                    }
                });
    }

    private void initTradPlus() {
        TradPlusSdk.initSdk(this, "42B52D8D365F0B178698D24A4B0EABFC");
    }

    private TPInterstitial mTradPlusInterstitial;

    private void testTradPlus() {
        mTradPlusInterstitial = new TPInterstitial(this, "66219211A2091DA4D8F2A9B3708D4D54");
        mTradPlusInterstitial.setAdListener(new InterstitialAdListener() {
            @Override
            public void onAdLoaded(TPAdInfo tpAdInfo) {
                Log.d(TAG, "onAdLoaded: " + tpAdInfo);
                mTradPlusInterstitial.showAd(MainActivity.this, null);
            }

            @Override
            public void onAdClicked(TPAdInfo tpAdInfo) {
                Log.d(TAG, "onAdClicked: " + tpAdInfo);
            }

            @Override
            public void onAdImpression(TPAdInfo tpAdInfo) {
                Log.d(TAG, "onAdImpression: " + tpAdInfo);
                // 使用 TradPlus SDK 加载广告，发送 w_ad_imp 事件到 Firebase
                // 在 TradPlus 广告的 onAdImpression() 回调中调用 send_w_ad_impForTradPlus()
                // 参数为 onAdImpression() 回调的 TPAdInfo 对象
                NoTaurusXHelper.send_w_ad_impForTradPlus(MainActivity.this, tpAdInfo, ADs.TYPE_INTERSTITIAL);
            }

            @Override
            public void onAdFailed(TPAdError tpAdError) {
                Log.d(TAG, "onAdFailed, code is: " + tpAdError.getErrorCode()
                        + ", message is: " + tpAdError.getErrorMsg());
            }

            @Override
            public void onAdClosed(TPAdInfo tpAdInfo) {
                Log.d(TAG, "onAdClosed: " + tpAdInfo);
            }

            @Override
            public void onAdVideoError(TPAdInfo tpAdInfo) {
                Log.d(TAG, "onAdVideoError: " + tpAdInfo);
            }
        });
        mTradPlusInterstitial.loadAd();
    }
}