package com.firebase.event.test;

import android.app.Application;
import android.os.Bundle;

import com.firebase.event.notaurusx.NoTaurusXHelper;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;
import java.util.Map;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // 使用 MoPub SDK 加载广告，发送 w_ad_imp 事件到 Firebase
        // 仅在 App 启动时调用一次
        NoTaurusXHelper.send_w_ad_impForMoPub(this);


        // 上报 w_withdraw 示例
        Bundle bundle = new Bundle();
        bundle.putString("item_id", "taskId");
        bundle.putString("value", "value");
        // 发送到 Firebase
        FirebaseAnalytics.getInstance(this).logEvent("w_withdraw", bundle);
    }
}
