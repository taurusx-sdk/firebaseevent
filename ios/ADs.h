#import <Foundation/Foundation.h>

/**
 * 定义Ads广告渠道
 * //, , 5 Applovin Bidding（max）, , , , , , ,
 * // , 13 Alt, 14 Senjoy, 15 Appnext, 16 BAT, ，18 Inneractive， ，，21 Spread(内部导流渠道), ,
 * // 23 Wemob
 */
typedef NS_ENUM(NSUInteger, ADs_SDK_Channel) {
    ADs_SDK_Channel_UNKNOWN = 0,
    ADs_SDK_Channel_ADCOLONY = 1, //9 Adcolony
    ADs_SDK_Channel_ADMOB = 2, //2 Admob
    ADs_SDK_Channel_APPLOVIN = 3, //4 Applovin
    ADs_SDK_Channel_CHARTBOOST = 4, //10 Chartboost
    ADs_SDK_Channel_FACEBOOK = 5, //3 FB
    ADs_SDK_Channel_IRON_SOURCE = 6, //8 Ironsource
    ADs_SDK_Channel_MOPUB = 7, //0 Mopub
    ADs_SDK_Channel_UNITY = 8, //6 Unity
    ADs_SDK_Channel_DSPMOB = 9, //22 Dspmob(内部广告平台)
    ADs_SDK_Channel_FYBER = 10, //18 Fyber
    ADs_SDK_Channel_INMOBI = 11, //17 Inmobi
    ADs_SDK_Channel_VUNGLE = 12, //11 Vungle
    ADs_SDK_Channel_DFP = 13, //1 Adx(dfp)
    ADs_SDK_Channel_CREATIVE = 14, //31
    ADs_SDK_Channel_DAP = 15, //32
    ADs_SDK_Channel_BAIDU = 16, //26
    ADs_SDK_Channel_DISPLAYIO = 17, //12 DisplayIO
    ADs_SDK_Channel_TOUTIAO = 18, //19 穿山甲
    ADs_SDK_Channel_GDT = 19, //20 广点通
    ADs_SDK_Channel_AMAZON = 20, //33
    ADs_SDK_Channel_FLURRY = 21, //34
    ADs_SDK_Channel_TAPJOY = 22, //35
    ADs_SDK_Channel_360 = 23, //24
    ADs_SDK_Channel_XIAOMI = 24, //30
    ADs_SDK_Channel_4399 = 25, //25
    ADs_SDK_Channel_OPPO = 26, //28
    ADs_SDK_Channel_VIVO = 27, //29
    ADs_SDK_Channel_MOBVISTA = 28, //27
    ADs_SDK_Channel_NEND = 29, //36
    ADs_SDK_Channel_ADGENERATION = 30, //38
    ADs_SDK_Channel_MAIO = 31, //39
    ADs_SDK_Channel_ALI_GAMES = 32, //40
    ADs_SDK_Channel_CRITEO = 33, //44
    ADs_SDK_Channel_ZHONGHUI_ADS = 34, //45
    ADs_SDK_Channel_TMS = 35, //46
    ADs_SDK_Channel_FIVE = 36, //47
    ADs_SDK_Channel_KUAI_SHOU = 37, //48
    ADs_SDK_Channel_IMOBILE = 38, //51
    ADs_SDK_Channel_PANGLE = 39, // 49
    ADs_SDK_Channel_SIGMOB = 40, // 52
    ADs_SDK_Channel_PREBID = 41, // 53
    ADs_SDK_Channel_OUPENG = 42, // 54
    ADs_SDK_Channel_APPNEXUS = 43, // 55
    ADs_SDK_Channel_IFLY = 44, // 56
    ADs_SDK_Channel_TUIA = 45, // 42
    ADs_SDK_Channel_YOUDAO = 46, // 57
    ADs_SDK_Channel_APPLOVIN_MAX = 47, // 5
    ADs_SDK_Channel_MOBRAIN = 48, // 37
    ADs_SDK_Channel_ALIBC = 49, //58
    ADs_SDK_Channel_QTT = 50, // 59
    ADs_SDK_Channel_LINKAI = 51, // 60
    ADs_SDK_Channel_JAD = 52, // 61
    ADs_SDK_Channel_YKY = 53, // 50
    ADs_SDK_Channel_TopOn = 54, // 62
    ADs_SDK_Channel_MeiShu = 55, // 64
    ADs_SDK_Channel_TradPlus = 56, // 63
    ADs_SDK_Channel_AppLovin_Exchange = 57 // 65
};

/**
 * 广告类型 from TaurusX
 */
typedef NS_ENUM(NSUInteger, TaurusX_Type) {
    TaurusX_Type_UNKNOWN = 0,
    TaurusX_Type_BANNER = 1,
    TaurusX_Type_INTERSTITIAL = 2,
    TaurusX_Type_NATIVE = 3,
    TaurusX_Type_REWARD = 4,
    TaurusX_Type_SPLASH = 7,
    TaurusX_Type_FEED_LIST = 8,
};



@interface ADs : NSObject

+ (NSString *)transType:(TaurusX_Type) type;
+ (NSString *)transSdkName:(NSString *)sdkName;

@end

