#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoTaurusXHelper : NSObject

+ (void)send_w_ad_impForMax:(MAAd *)maxAd;

@end

NS_ASSUME_NONNULL_END
