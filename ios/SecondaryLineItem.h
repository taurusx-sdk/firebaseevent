#import <Foundation/Foundation.h>
#import "ADs_Network.h"
@class Builder;
@class SecondaryLineItem;

@interface Builder : NSObject

@property(nonatomic,assign)ADs_Network_Type mNetwork;
@property(nonatomic,strong)NSString *mAdUnitId;
@property(nonatomic,assign)double meCPM;

- (SecondaryLineItem *)build;

@end


@interface SecondaryLineItem : NSObject

@property(nonatomic,assign,readonly)ADs_Network_Type network;
@property(nonatomic,strong,readonly)NSString *adUnitId;
@property(nonatomic,assign,readonly)double eCPM;

+ (Builder *)newBuilder;

@end
