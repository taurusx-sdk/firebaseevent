#import "ADs.h"

/**
 * 定义Ads广告类型
 * 广告类型编号
 */
typedef NS_ENUM(NSUInteger, ADs_SDK_Type) {
    //广告类型编号
    ADs_SDK_Type_BANNER = 0,
    ADs_SDK_Type_INTERSTITIAL = 1,
    ADs_SDK_Type_REWARD = 2,
    ADs_SDK_Type_NATIVE = 3,
    ADs_SDK_Type_UNKNOWN = 4,
    ADs_SDK_Type_SPLASH = 5,
    ADs_SDK_Type_FEED_LIST = 6
};


#import "ADs.h"

@implementation ADs

+ (NSString *)transType:(TaurusX_Type) type{
    
    ADs_SDK_Type transType = ADs_SDK_Type_BANNER;
    
    switch (type) {
        case TaurusX_Type_BANNER:
            transType = ADs_SDK_Type_BANNER; //0
            break;
        case TaurusX_Type_INTERSTITIAL:
            transType = ADs_SDK_Type_INTERSTITIAL; //1
            break;
        case TaurusX_Type_REWARD:
            transType = ADs_SDK_Type_REWARD; //2
            break;
        case TaurusX_Type_NATIVE:
            transType = ADs_SDK_Type_NATIVE; //3
            break;
        case TaurusX_Type_SPLASH:
            transType = ADs_SDK_Type_SPLASH; //5
            break;
        case TaurusX_Type_FEED_LIST:
            transType = ADs_SDK_Type_FEED_LIST; //6
            break;
        default:
            transType = ADs_SDK_Type_UNKNOWN; //4
            break;
    }
    
    return [NSString stringWithFormat:@"%ld",transType];
}

+ (NSString *)transSdkName:(NSString *)sdkName{
    NSString *transSdkName = nil;
    
    switch ([sdkName intValue]) {
        case ADs_SDK_Channel_MOPUB:
            transSdkName = @"0";
            break;
        case ADs_SDK_Channel_DFP:
            transSdkName = @"1";
            break;
        case ADs_SDK_Channel_ADMOB:
            transSdkName = @"2";
            break;
        case ADs_SDK_Channel_FACEBOOK:
            transSdkName = @"3";
            break;
        case ADs_SDK_Channel_APPLOVIN:
            transSdkName = @"4";
            break;
        case ADs_SDK_Channel_APPLOVIN_MAX:
            transSdkName = @"5";
            break;
        case ADs_SDK_Channel_UNITY:
            transSdkName = @"6";
            break;
        case ADs_SDK_Channel_IRON_SOURCE:
            transSdkName = @"8";
            break;
        case ADs_SDK_Channel_ADCOLONY:
            transSdkName = @"9";
            break;
        case ADs_SDK_Channel_CHARTBOOST:
            transSdkName = @"10";
            break;
        case ADs_SDK_Channel_VUNGLE:
            transSdkName = @"11";
            break;
        case ADs_SDK_Channel_DISPLAYIO:
            transSdkName = @"12";
            break;
        case ADs_SDK_Channel_INMOBI:
            transSdkName = @"17";
            break;
        case ADs_SDK_Channel_FYBER:
            transSdkName = @"18";
            break;
        case ADs_SDK_Channel_TOUTIAO:
            transSdkName = @"19";
            break;
        case ADs_SDK_Channel_GDT:
            transSdkName = @"20";
            break;
        case ADs_SDK_Channel_DSPMOB:
            transSdkName = @"22";
            break;
        case ADs_SDK_Channel_360:
            transSdkName = @"24";
            break;
        case ADs_SDK_Channel_4399:
            transSdkName = @"25";
            break;
        case ADs_SDK_Channel_BAIDU:
            transSdkName = @"26";
            break;
        case ADs_SDK_Channel_MOBVISTA:
            transSdkName = @"27";
            break;
        case ADs_SDK_Channel_OPPO:
            transSdkName = @"28";
            break;
        case ADs_SDK_Channel_VIVO:
            transSdkName = @"29";
            break;
        case ADs_SDK_Channel_XIAOMI:
            transSdkName = @"30";
            break;
        case ADs_SDK_Channel_CREATIVE:
            transSdkName = @"31";
            break;
        case ADs_SDK_Channel_DAP:
            transSdkName = @"32";
            break;
        case ADs_SDK_Channel_AMAZON:
            transSdkName = @"33";
            break;
        case ADs_SDK_Channel_FLURRY:
            transSdkName = @"34";
            break;
        case ADs_SDK_Channel_TAPJOY:
            transSdkName = @"35";
            break;
        case ADs_SDK_Channel_NEND:
            transSdkName = @"36";
            break;
        case ADs_SDK_Channel_MOBRAIN:
            transSdkName = @"37";
            break;
        case ADs_SDK_Channel_ADGENERATION:
            transSdkName = @"38";
            break;
        case ADs_SDK_Channel_MAIO:
            transSdkName = @"39";
            break;
        case ADs_SDK_Channel_ALI_GAMES:
            transSdkName = @"40";
            break;
        case ADs_SDK_Channel_TUIA:
            transSdkName = @"42";
            break;
        case ADs_SDK_Channel_CRITEO:
            transSdkName = @"44";
            break;
        case ADs_SDK_Channel_ZHONGHUI_ADS:
            transSdkName = @"45";
            break;
        case ADs_SDK_Channel_TMS:
            transSdkName = @"46";
            break;
        case ADs_SDK_Channel_FIVE:
            transSdkName = @"47";
            break;
        case ADs_SDK_Channel_KUAI_SHOU:
            transSdkName = @"48";
            break;
        case ADs_SDK_Channel_PANGLE:
            transSdkName = @"49";
            break;
        case ADs_SDK_Channel_YKY:
            transSdkName = @"50";
            break;
        case ADs_SDK_Channel_IMOBILE:
            transSdkName = @"51";
            break;
        case ADs_SDK_Channel_SIGMOB:
            transSdkName = @"52";
            break;
        case ADs_SDK_Channel_PREBID:
            transSdkName = @"53";
            break;
        case ADs_SDK_Channel_OUPENG:
            transSdkName = @"54";
            break;
        case ADs_SDK_Channel_APPNEXUS:
            transSdkName = @"55";
            break;
        case ADs_SDK_Channel_IFLY:
            transSdkName = @"56";
            break;
        case ADs_SDK_Channel_YOUDAO:
            transSdkName = @"57";
            break;
        case ADs_SDK_Channel_ALIBC:
            transSdkName = @"58";
            break;
        case ADs_SDK_Channel_QTT:
            transSdkName = @"59";
            break;
        case ADs_SDK_Channel_LINKAI:
            transSdkName = @"60";
            break;
        case ADs_SDK_Channel_JAD:
            transSdkName = @"61";
            break;
        case ADs_SDK_Channel_TopOn:
            transSdkName = @"62";
            break;
        case ADs_SDK_Channel_MeiShu:
            transSdkName = @"64";
            break;
        case ADs_SDK_Channel_TradPlus:
            transSdkName = @"63";
            break;
        case ADs_SDK_Channel_AppLovin_Exchange:
            transSdkName = @"65";
            break;
        default:
            transSdkName = @"99"; //UNKNOWN
            break;
    }
    return transSdkName;
}


@end
