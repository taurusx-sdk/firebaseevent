# iOS 使用 Embed 打点：w_ad_imp
本文介绍直接接入 Max SDK 时，如何通过 Embed SDK 进行打点。

## 添加 SDK
### CocoaPods（首选）
最简便的方法就是使用 CocoaPods。请打开项目的 Podfile 并将下面这行代码添加到应用的目标中：
pod 'EmbededSdk'

然后使用命令行运行：
pod install --repo-update

### 手动下载
直接下载并解压缩 SDK 框架，然后将以下框架导入您的 Xcode 项目中：

- EmbededSdk.framework (https://github.com/webeyemob/TaurusXAds_iOS_Pub/raw/master/EmbededSdk/EmbededSdk_1.2.2.zip)
- EventsIOSdk.framework (https://github.com/webeyemob/TaurusXAds_iOS_Pub/raw/master/EventsIOSdk/EventsIOSdk_1.1.0.zip)
- TaurusXAds.framework (https://github.com/webeyemob/TaurusXAds_iOS_Pub/raw/master/TaurusXAds/TaurusXAds_2.1.9.zip)

## 配置 apple id
在 plist 里配置apple id，key 为 "TGCAppleID", value为 "id+应用在苹果商店上架的appleid"

## 添加 -ObjC
将 -ObjC 链接器标记添加到项目的 Build Settings 下的 Other Linker Flags 中。

## 初始化 SDK
初始化之后，SDK 就会自动采集一些基本事件。

```c
[Embeded setOverSea:YES];
[Embeded start];
```

## 发送 w_ad_imp 事件
### 使用 AppLovin Max 加载广告
- 请将此文件夹下的代码拷贝到项目中：https://bitbucket.org/taurusx-sdk/firebaseevent/src/master/ios

- 在所有 Max 广告的 didPayRevenueForAd 回调中调用 [NoTaurusXHelper send_w_ad_impForMax:ad]，来发送 w_ad_imp 事件。

```c
- (void)didPayRevenueForAd:(MAAd *)ad
{
    [NoTaurusXHelper send_w_ad_impForMax:ad];
}
```