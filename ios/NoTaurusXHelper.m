#import <EventsIOSdk/EventsIOSdk.h>
#import <AppLovinSDK/AppLovinSDK.h>

#import "NoTaurusXHelper.h"
#import "ADs.h"
#import "MaxHelper.h"

@implementation NoTaurusXHelper

+ (void)send_w_ad_impForMax:(MAAd *)maxAd{
    if (!maxAd) {
        return;
    }
    
    NSString *sdkName = [NSString stringWithFormat:@"%ld",ADs_SDK_Channel_APPLOVIN_MAX];
    TaurusX_Type type = [MaxHelper getAdType:maxAd];
    
    NSMutableDictionary *bundle = [NSMutableDictionary dictionaryWithDictionary:@{
        @"sdk_name":[ADs transSdkName:sdkName],
        @"type":[ADs transType:type]
    }];
    
    if (maxAd.adUnitIdentifier && maxAd.adUnitIdentifier.length > 0) {
        [bundle setObject:maxAd.adUnitIdentifier forKey:@"pid"];
    }
    
    @try {
        SecondaryLineItem *secondaryLineItem = [MaxHelper generateSecondaryLineItem:@"NoTaurusXHelper" maxAd:maxAd];
        if (secondaryLineItem != nil) {
            NSString *secSdkName = [NSString stringWithFormat:@"%ld",secondaryLineItem.network];
            [bundle setObject:[ADs transSdkName:secSdkName] forKey:@"m_sdk_name"];
            [bundle setObject:secondaryLineItem.adUnitId forKey:@"m_pid"];
            [bundle setObject:[NSString stringWithFormat:@"%f",secondaryLineItem.eCPM] forKey:@"m_ecpm"];
        }
    } @catch (NSException *exception) {
    } @finally {
        
    }
    [EventIOHolder trackEvent:@"w_ad_imp" eventValue:bundle];
    
}

@end
