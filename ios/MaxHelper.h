#import <Foundation/Foundation.h>
#import "ADs.h"
#import "SecondaryLineItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface MaxHelper : NSObject

+ (SecondaryLineItem *)generateSecondaryLineItem:(NSString *)tag maxAd:(MAAd *)maxAd;
+ (TaurusX_Type)getAdType:(MAAd *)maxAd;

@end

NS_ASSUME_NONNULL_END
