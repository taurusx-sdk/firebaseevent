#import "SecondaryLineItem.h"
#import "ADs_Network.h"

@interface SecondaryLineItem ()

@property(nonatomic,strong)Builder *builder;



@end
@implementation SecondaryLineItem

- (instancetype)initWith:(Builder *)builder{
    self = [super init];
    if (self) {
        self.builder = builder;
    }
    return self;
}

+ (Builder *)newBuilder{
    return [[Builder alloc] init];
}

- (ADs_Network_Type)network{
    return self.builder.mNetwork;
}

- (NSString *)adUnitId{
    return self.builder.mAdUnitId;
}

- (double)eCPM{
    return self.builder.meCPM;
}

@end



@implementation Builder

- (SecondaryLineItem *)build{
    return [[SecondaryLineItem alloc] initWith:self];
}


@end
