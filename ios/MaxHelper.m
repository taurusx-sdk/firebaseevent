#import <AppLovinSDK/AppLovinSDK.h>
#import "MaxHelper.h"
#import "ADs_Network.h"
#import <UIKit/UIKit.h>
@implementation MaxHelper

+ (SecondaryLineItem *)generateSecondaryLineItem:(NSString *)tag maxAd:(MAAd *)maxAd{
    ADs_Network_Type network = [MaxHelper fromNetworkName:maxAd.networkName];
    NSString *adUnitId = network == ADs_NetWork_APPLOVIN || network == ADs_NetWork_APPLOVIN_EXCHANGE ? maxAd.adUnitIdentifier : maxAd.networkPlacement;
    double eCPM = maxAd.revenue > 0 ? maxAd.revenue * 1000 : 0;
    Builder *build = [SecondaryLineItem newBuilder];
    build.mNetwork = network;
    build.mAdUnitId = adUnitId;
    build.meCPM = eCPM;
    return [build build];
}

+ (ADs_Network_Type)fromNetworkName:(NSString *)name{
    if ([[name lowercaseString] containsString:@"facebook"]) {
        return ADs_NetWork_FACEBOOK;
    }
    if ([[name lowercaseString] containsString:@"verve"]) {
        return ADs_NetWork_VERVE;
    }
    
    if ([name isEqualToString: @"AdColony"]){
        return ADs_NetWork_ADCOLONY;
    } else if ([name isEqualToString: @"Google AdMob"]){
        return ADs_NetWork_ADMOB;
    } else if ([name isEqualToString: @"AppLovin"]){
        return ADs_NetWork_APPLOVIN;
    } else if ([name isEqualToString: @"Chartboost"]){
        return ADs_NetWork_CHARTBOOST;
    } else if ([name isEqualToString: @"Fyber"]){
        return ADs_NetWork_FYBER;
    } else if ([name isEqualToString: @"Google Ad Manager"]){
        return ADs_NetWork_DFP;
    } else if ([name isEqualToString: @"InMobi"]){
        return ADs_NetWork_INMOBI;
    } else if ([name isEqualToString: @"IronSource"]){
        return ADs_NetWork_IRON_SOURCE;
    } else if ([name isEqualToString: @"Maio"]){
        return ADs_NetWork_MAIO;
    } else if ([name isEqualToString: @"Mintegral"]){
        return ADs_NetWork_MINTEGRAL;
    } else if ([name isEqualToString: @"MoPub"]){
        return ADs_NetWork_MOPUB;
    } else if ([name isEqualToString: @"Nend"]){
        return ADs_NetWork_NEND;
    } else if ([name isEqualToString: @"Pangle"]){
        return ADs_NetWork_PANGLE;
    } else if ([name isEqualToString: @"Tapjoy"]){
        return ADs_NetWork_TAPJOY;
    } else if ([name isEqualToString: @"Unity Ads"]){
        return ADs_NetWork_UNITY;
    } else if ([name isEqualToString: @"Vungle"]){
        return ADs_NetWork_VUNGLE;
    } else if ([name isEqualToString: @"APPLOVIN_EXCHANGE"]){
        return ADs_NetWork_APPLOVIN_EXCHANGE;
    }else{
        return ADs_NetWork_UNKNOWN;
    }
    
}

+ (TaurusX_Type)getAdType:(MAAd *)maxAd{
    MAAdFormat *maxAdFormat = maxAd.format;
    NSString *label = maxAdFormat.label;
    
    TaurusX_Type type = TaurusX_Type_UNKNOWN;
    if ([MAAdFormat.banner.label isEqualToString:label] ||
        [MAAdFormat.mrec.label isEqualToString:label] ||
        [MAAdFormat.leader.label isEqualToString:label] ) {
        type = TaurusX_Type_BANNER;
    }else if ([MAAdFormat.interstitial.label isEqualToString:label]){
        type = TaurusX_Type_INTERSTITIAL;
    }else if ([MAAdFormat.native.label isEqualToString:label]){
        type = TaurusX_Type_NATIVE;
    }else if ([MAAdFormat.rewarded.label isEqualToString:label]){
        type = TaurusX_Type_REWARD;
    }
    return type;
}

@end
