# 往 Firebase 打点：w_withdraw、w_ad_imp
本文介绍如何通过 Firebase SDK 进行事件的打点。

## 获取 appsflyer_device_id 和 OpenUdid 并上报到 Firebase
1、需要添加 AppsFlyer/Adjust SDK，参考对应的的官方集成文档。

2、添加 OpenUdid SDK，如下所示。
配置项目级别的 gradle 文件，添加 maven 仓库。
```java
allprojects {
    repositories {
        maven { url "https://bitbucket.org/sdkcenter/sdkcenter/raw/release" }
    }
}
```
配置应用级别 gradle，添加 SDK 依赖。
```java
dependencies {
    implementation "com.satori.sdk:eventio-openudid:1.1.7"
}
```

3、上报 Adjust/AppsFlyer Id。

3.1、如果集成 Adjust SDK，初始化 Adjust 并上报 Id。（不需添加 3.2 代码）
```java
private void initAdjust() {
    String appToken = "{YourAppToken}";
    String environment = ...;
    AdjustConfig config = new AdjustConfig(this, appToken, environment);
    Adjust.onCreate(config);

    // 上报 Adjust Id（集成 Adjust SDK 时调用）
    FirebaseAnalytics.getInstance(context).setUserProperty("appsflyer_device_id",
            Adjust.getAdid());
}
```

3.2、如果没有集成 Adjust SDK，初始化 AppsFlyer 并在 onConversionDataSuccess() 回调中上报。（不需要添加 3.1 代码）
```java
private void initAppsFlyer() {
    AppsFlyerConversionListener conversionDataListener = new AppsFlyerConversionListener() {
        @Override
        public void onConversionDataSuccess(Map<String, Object> conversionData) {
            try {
                if (conversionData.containsKey("af_status")) {
                    // 上报 AppsFlyer Id（没有集成 Adjust SDK 时调用）
                    FirebaseAnalytics.getInstance(context).setUserProperty("appsflyer_device_id",
                            AppsFlyerLib.getInstance().getAppsFlyerUID(context));
                }
            } catch (Exception | Error e) {
            }
        }

        @Override
        public void onConversionDataFail(String s) {
        }

        @Override
        public void onAppOpenAttribution(Map<String, String> attributionData) {
        }

        @Override
        public void onAttributionFailure(String errorMessage) {
        }
    };

    AppsFlyerLib.getInstance().registerConversionListener(getApplicationContext(), conversionDataListener);
    AppsFlyerLib.getInstance().init("AppsFlyer DevKey", conversionDataListener, context);
    AppsFlyerLib.getInstance().start(this);
}
```

4、上报 OpenUdid
```java
// 上报 OpenUdid
FirebaseAnalytics.getInstance(context).setUserProperty("openudid",
        OpenUDIDClient.getOpenUDID(context));
```

## w_withdraw（非网赚产品请忽略）
```java
// 上报 w_withdraw 示例
Bundle bundle = new Bundle();
bundle.putString("item_id", "taskId"); // 提现任务 Id
bundle.putString("value", "value"); // 提现金额
bundle.putString("type", "IDR"); // 币种
// 发送到 Firebase
FirebaseAnalytics.getInstance(this).logEvent("w_withdraw", bundle);
```

## w_ad_imp
### 使用 TaurusX SDK 加载广告
- 请将此文件夹下的代码拷贝到项目中：https://bitbucket.org/taurusx-sdk/firebaseevent/src/master/taurusx/src/main/java/com/firebase/event/taurusx/
- 然后，调用 TaurusXHelper.send_w_ad_imp() 方法，请确保只调用一次。此方法会自动在广告展示时发送 w_ad_imp 事件到 Firebase。
```java
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // 使用 TaurusX SDK 加载广告，发送 w_ad_imp 事件到 Firebase
        // 仅在 App 启动时调用一次
        TaurusXHelper.send_w_ad_imp(this);
    }
}
```

### 不使用 TaurusX SDK 加载广告
- 请将此文件夹下的代码拷贝到项目中：https://bitbucket.org/taurusx-sdk/firebaseevent/src/master/notaurusx/src/main/java/com/firebase/event/notaurusx/
- 支持 MoPub、Max、 AdMob 三个平台，每个平台的使用方法不同，具体请看下面的代码：
#### MoPub
调用 NoTaurusXHelper.send_w_ad_impForMoPub() 方法，请确保只调用一次。此方法会自动在 MoPub 广告展示时发送 w_ad_imp 事件到 Firebase。
示例如下：
```java
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // 使用 MoPub SDK 加载广告，发送 w_ad_imp 事件到 Firebase
        // 仅在 App 启动时调用一次
        NoTaurusXHelper.send_w_ad_impForMoPub(this);
    }
}
```

#### Applovin Max
在所有 Max 广告的 onAdRevenuePaid() 回调中调用 NoTaurusXHelper.send_w_ad_impForMax()，来发送 w_ad_imp 事件到 Firebase。
示例如下：
```java
private void testMax() {
    mMaxInterstitialAd = new MaxInterstitialAd("9c24dcebe57764e3", this);
    mMaxInterstitialAd.setListener(new MaxAdListener() {
        ...
    });
    mMaxInterstitialAd.setRevenueListener(new MaxAdRevenueListener() {
        @Override
        public void onAdRevenuePaid(MaxAd maxAd) {
            Log.d(TAG, "onAdRevenuePaid");
            // 使用 Max SDK 加载广告，发送 w_ad_imp 事件到 Firebase
            // 在所有 Max 广告的 onAdRevenuePaid() 回调中调用 send_w_ad_impForMax()
            // 参考：https://dash.applovin.com/documentation/mediation/android/getting-started/advanced-settings
            NoTaurusXHelper.send_w_ad_impForMax(MainActivity.this, maxAd);
        }
    });
    mMaxInterstitialAd.loadAd();
}
```

#### AdMob
在所有 AdMob 广告的 onAdImpression() 回调中调用 NoTaurusXHelper.send_w_ad_impForAdMob()，来发送 w_ad_imp 事件到 Firebase。
示例如下：
```java
InterstitialAd.load(this,
        "ca-app-pub-3940256099942544/1033173712",
        builder.build(),
        new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                Log.d(TAG, "onAdLoaded");

                mAdMobInterstitialAd = interstitialAd;
                mAdMobInterstitialAd.setImmersiveMode(true);
                mAdMobInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdShowedFullScreenContent() {
                        Log.d(TAG, "onAdShowedFullScreenContent");
                    }

                    @Override
                    public void onAdImpression() {
                        Log.d(TAG, "onAdImpression");
                        // 使用 AdMob SDK 加载广告，发送 w_ad_imp 事件到 Firebase
                        // 在 AdMob 广告的 onAdImpression() 回调中调用 send_w_ad_impForAdMob()
                        // 参数通过调用 AdMob InterstitialAd/RewardedAd 的 getAdUnitId()、getResponseInfo() 获得
                        NoTaurusXHelper.send_w_ad_impForAdMob(MainActivity.this,
                                mAdMobInterstitialAd.getAdUnitId(),
                                mAdMobInterstitialAd.getResponseInfo());
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        Log.d(TAG, "onAdFailedToShowFullScreenContent");
                    }

                    @Override
                    public void onAdDismissedFullScreenContent() {
                        Log.d(TAG, "onAdDismissedFullScreenContent");
                    }
                });
                mAdMobInterstitialAd.show(MainActivity.this);
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                Log.e(TAG, "onAdFailedToLoad");
            }
        });
```

#### TradPlus
在所有 TradPlus 广告的 onAdImpression() 回调中调用 send_w_ad_impForTradPlus()，来发送 w_ad_imp 事件到 Firebase。
示例如下：
```java
mTradPlusInterstitial = new TPInterstitial(this, "66219211A2091DA4D8F2A9B3708D4D54");
mTradPlusInterstitial.setAdListener(new InterstitialAdListener() {
    @Override
    public void onAdLoaded(TPAdInfo tpAdInfo) {
        Log.d(TAG, "onAdLoaded: " + tpAdInfo);
        mTradPlusInterstitial.showAd(MainActivity.this, null);
    }

    @Override
    public void onAdClicked(TPAdInfo tpAdInfo) {
        Log.d(TAG, "onAdClicked: " + tpAdInfo);
    }

    @Override
    public void onAdImpression(TPAdInfo tpAdInfo) {
        Log.d(TAG, "onAdImpression: " + tpAdInfo);
        // 使用 TradPlus SDK 加载广告，发送 w_ad_imp 事件到 Firebase
        // 在 TradPlus 广告的 onAdImpression() 回调中调用 send_w_ad_impForTradPlus()
        // 参数为 onAdImpression() 回调的 TPAdInfo 对象
        NoTaurusXHelper.send_w_ad_impForTradPlus(MainActivity.this, tpAdInfo);
    }

    @Override
    public void onAdFailed(TPAdError tpAdError) {
        Log.d(TAG, "onAdFailed, code is: " + tpAdError.getErrorCode()
                + ", message is: " + tpAdError.getErrorMsg());
    }

    @Override
    public void onAdClosed(TPAdInfo tpAdInfo) {
        Log.d(TAG, "onAdClosed: " + tpAdInfo);
    }

    @Override
    public void onAdVideoError(TPAdInfo tpAdInfo) {
        Log.d(TAG, "onAdVideoError: " + tpAdInfo);
    }
});
mTradPlusInterstitial.loadAd();
```

# 往 Embed 打点：w_ad_imp
## 添加 Embed SDK，如下所示。
配置项目级别的 gradle 文件，添加 maven 仓库。
```java
allprojects {
    repositories {
        maven { url "https://bitbucket.org/sdkcenter/sdkcenter/raw/release" }
    }
}
```
配置应用级别 gradle，添加 SDK 依赖。
```java
dependencies {
    implementation "we.studio.embed:embed-sdk:6.3.5"
    implementation "com.satori.sdk:eventio-go-oversea:2.3.7.1"
    implementation "com.satori.sdk:eventio-openudid:1.1.7"
    implementation "com.satori.sdk:eventio-crashlytics:1.0.7"
    implementation "com.google.android.gms:play-services-cronet:17.0.0"
}
```

初始化 Embed SDK：
```java
EmbedSDK.getInstance().init(context);
```

上面的代码中的 NoTaurusXHelper 是往 Firebase 打点的，将其替换为 NoTaurusXEmbedHelper 即可往 Embed 打点。
以 Max 举例，如下：

#### Applovin Max
在所有 Max 广告的 onAdRevenuePaid() 回调中调用 NoTaurusXEmbedHelper.send_w_ad_impForMax()，来发送 w_ad_imp 事件到 Embed。

```java
private void testMax() {
    mMaxInterstitialAd = new MaxInterstitialAd("9c24dcebe57764e3", this);
    mMaxInterstitialAd.setListener(new MaxAdListener() {
        ...
    });
    mMaxInterstitialAd.setRevenueListener(new MaxAdRevenueListener() {
        @Override
        public void onAdRevenuePaid(MaxAd maxAd) {
            Log.d(TAG, "onAdRevenuePaid");
            // 使用 Max SDK 加载广告，发送 w_ad_imp 事件到 Embed
            // 在所有 Max 广告的 onAdRevenuePaid() 回调中调用 send_w_ad_impForMax()
            // 参考：https://dash.applovin.com/documentation/mediation/android/getting-started/advanced-settings
            NoTaurusXEmbedHelper.send_w_ad_impForMax(MainActivity.this, maxAd);
        }
    });
    mMaxInterstitialAd.loadAd();
}
```