package com.firebase.event.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.firebase.event.R;
import com.taurusx.ads.core.api.TaurusXAds;
import com.taurusx.ads.core.api.ad.InterstitialAd;
import com.taurusx.ads.core.api.listener.newapi.AdListener;
import com.taurusx.ads.core.api.model.ILineItem;
import com.taurusx.ads.core.api.requestfilter.LineItemFilter;

/**
 * 测试使用 TaurusX 加载&展示广告。
 */
public class MainActivity extends Activity {

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TaurusXAds.getDefault().setLogEnable(true);
        TaurusXAds.getDefault().init(this, "4b4b6832-4267-42db-9c04-d517d5288bbb");
        TaurusXAds.getDefault().setLineItemFilter(new LineItemFilter() {
            @Override
            public boolean accept(ILineItem iLineItem) {
                return iLineItem.getNetworkAdUnitId().equals("9116ec36c196443d965c5b010e6cc053");
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("1fa4f6cd-d817-45db-aef4-7ccd0c54c128");
        mInterstitialAd.setADListener(new AdListener() {
            @Override
            public void onAdLoaded(ILineItem lineItem) {
                mInterstitialAd.show(MainActivity.this);
            }
        });

        findViewById(R.id.button_load).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.loadAd();
            }
        });
    }
}