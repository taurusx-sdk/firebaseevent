package com.firebase.event.test;

import android.app.Application;
import android.os.Bundle;

import com.firebase.event.taurusx.TaurusXHelper;
import com.google.firebase.analytics.FirebaseAnalytics;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // 使用 TaurusX SDK 加载广告，发送 w_ad_imp 事件到 Firebase
        // 仅在 App 启动时调用一次
        TaurusXHelper.send_w_ad_imp(this);

        // 上报 w_withdraw 示例
        Bundle bundle = new Bundle();
        bundle.putString("item_id", "taskId");
        bundle.putString("value", "value");
        // 发送到 Firebase
        FirebaseAnalytics.getInstance(this).logEvent("w_withdraw", bundle);
    }
}
