package com.firebase.event.taurusx;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.taurusx.ads.core.api.model.ILineItem;
import com.taurusx.ads.core.api.model.SecondaryLineItem;
import com.taurusx.ads.core.api.tracker.SimpleTrackerListener;
import com.taurusx.ads.core.api.tracker.TaurusXAdsTracker;
import com.taurusx.ads.core.api.tracker.TrackerInfo;

public class TaurusXHelper {

    // 使用 TaurusX SDK 加载广告，发送 w_ad_imp 事件到 Firebase
    // 仅在 App 启动时调用一次
    public static void send_w_ad_imp(Context context) {
        TaurusXAdsTracker.getInstance().registerListener(new SimpleTrackerListener() {
            @Override
            public void onAdShown(TrackerInfo info) {
                ILineItem lineItem = info.getLineItem();
                String eCpm = String.valueOf(lineItem.getEcpm());
                String sdkName = String.valueOf(lineItem.getNetwork().getNetworkId());
                String pid = lineItem.getNetworkAdUnitId();
                String type = String.valueOf(lineItem.getAdType().getType());
                String source = lineItem.getAdUnit().getId();
                String itemId = info.getSceneId();

                Bundle bundle = new Bundle();
                if (!TextUtils.isEmpty(source)) {
                    bundle.putString("source", source);
                }
                if (!TextUtils.isEmpty(sdkName)) {
                    bundle.putString("sdk_name", ADs.transSdkName(sdkName));
                }
                if (!TextUtils.isEmpty(pid)) {
                    bundle.putString("pid", pid);
                }
                if (!TextUtils.isEmpty(type)) {
                    bundle.putString("type", ADs.transType(type));
                }
                if (!TextUtils.isEmpty(eCpm)) {
                    bundle.putString("ecpm", eCpm);
                }
                if (!TextUtils.isEmpty(itemId)) {
                    bundle.putString("item_id", itemId);
                }
                try {
                    SecondaryLineItem secondaryLineItem = info.getSecondaryLineItem();
                    if (secondaryLineItem != null) {
                        String secSdkName = String.valueOf(secondaryLineItem.getNetwork().getNetworkId());
                        bundle.putString("m_sdk_name", ADs.transSdkName(secSdkName));
                        bundle.putString("m_pid", secondaryLineItem.getAdUnitId());
                        bundle.putString("m_ecpm", String.valueOf(secondaryLineItem.geteCPM()));
                    }
                } catch (Exception | Error e) {
                    e.printStackTrace();
                }

                // 发送到 Firebase
                FirebaseAnalytics.getInstance(context).logEvent("w_ad_imp", bundle);
            }
        });
    }
}