package com.firebase.event.taurusx;

public class ADs {
    //, , 5 Applovin Bidding（max）, , , , , , ,
    // , 13 Alt, 14 Senjoy, 15 Appnext, 16 BAT, ，18 Inneractive， ，，21 Spread(内部导流渠道), ,
    // 23 Wemob
    static final String AD_SDK_UNKNOWN = "0";
    static final String AD_SDK_ADCOLONY = "1"; //9 Adcolony
    static final String AD_SDK_ADMOB = "2"; //2 Admob
    static final String AD_SDK_APPLOVIN = "3"; //4 Applovin
    static final String AD_SDK_CHARTBOOST = "4"; //10 Chartboost
    static final String AD_SDK_FACEBOOK = "5"; //3 FB
    static final String AD_SDK_IRON_SOURCE = "6"; //8 Ironsource
    static final String AD_SDK_MOPUB = "7"; //0 Mopub
    static final String AD_SDK_UNITY = "8"; //6 Unity
    static final String AD_SDK_DSPMOB = "9"; //22 Dspmob(内部广告平台)
    static final String AD_SDK_FYBER = "10"; //18 Fyber
    static final String AD_SDK_INMOBI = "11"; //17 Inmobi
    static final String AD_SDK_VUNGLE = "12"; //11 Vungle
    static final String AD_SDK_DFP = "13"; //1 Adx(dfp)
    static final String AD_SDK_CREATIVE = "14"; //31
    static final String AD_SDK_DAP = "15"; //32
    static final String AD_SDK_BAIDU = "16"; //26
    static final String AD_SDK_DISPLAYIO = "17"; //12 DisplayIO
    static final String AD_SDK_TOUTIAO = "18"; //19 穿山甲
    static final String AD_SDK_GDT = "19"; //20 广点通
    static final String AD_SDK_AMAZON = "20"; //33
    static final String AD_SDK_FLURRY = "21"; //34
    static final String AD_SDK_TAPJOY = "22"; //35
    static final String AD_SDK_360 = "23"; //24
    static final String AD_SDK_XIAOMI = "24"; //30
    static final String AD_SDK_4399 = "25"; //25
    static final String AD_SDK_OPPO = "26"; //28
    static final String AD_SDK_VIVO = "27"; //29
    static final String AD_SDK_MOBVISTA = "28"; //27
    static final String AD_SDK_NEND = "29"; //36
    static final String AD_SDK_ADGENERATION = "30"; //38
    static final String AD_SDK_MAIO = "31"; //39
    static final String AD_SDK_ALI_GAMES = "32"; //40
    static final String AD_SDK_CRITEO = "33"; //44
    static final String AD_SDK_ZHONGHUI_ADS = "34"; //45
    static final String AD_SDK_TMS = "35"; //46
    static final String AD_SDK_FIVE = "36"; //47
    static final String AD_SDK_KUAI_SHOU = "37"; //48
    static final String AD_SDK_IMOBILE = "38"; //51
    static final String AD_SDK_PANGLE = "39"; // 49
    static final String AD_SDK_SIGMOB = "40"; // 52
    static final String AD_SDK_PREBID = "41"; // 53
    static final String AD_SDK_OUPENG = "42"; // 54
    static final String AD_SDK_APPNEXUS = "43"; // 55
    static final String AD_SDK_IFLY = "44"; // 56
    static final String AD_SDK_TUIA = "45"; // 42
    static final String AD_SDK_YOUDAO = "46"; // 57
    static final String AD_SDK_APPLOVIN_MAX = "47"; // 5
    static final String AD_SDK_MOBRAIN = "48"; // 37
    static final String AD_SDK_ALIBC = "49"; //58
    static final String AD_SDK_QTT = "50"; // 59
    static final String AD_SDK_LINKAI = "51"; // 60
    static final String AD_SDK_JAD = "52"; // 61
    static final String AD_SDK_YKY = "53"; // 50
    static final String AD_SDK_TopOn = "54"; // 62
    static final String AD_SDK_MeiShu = "55"; // 64
    static final String AD_SDK_TradPlus = "56"; // 63
    static final String AD_SDK_AppLovin_Exchange = "57"; // 65
    static final String AD_SDK_SAN = "58"; // 66
    static final String AD_SDK_VERVE = "59"; // 67
    static final String AD_SDK_MINT = "60"; //
    static final String AD_SDK_ADFLY = "61"; // 68

    //广告类型编号
    static final String AD_TYPE_BANNER = "0";
    static final String AD_TYPE_INTERSTITIAL = "1";
    static final String AD_TYPE_REWARD = "2";
    static final String AD_TYPE_NATIVE = "3";
    static final String AD_TYPE_UNKNOWN = "4";
    static final String AD_TYPE_SPLASH = "5";
    static final String AD_TYPE_FEED_LIST = "6";

    //广告类型 from wesdk
    static final String TYPE_UNKNOWN = "0";
    static final String TYPE_BANNER = "1";
    static final String TYPE_INTERSTITIAL = "2";
    static final String TYPE_NATIVE = "3";
    static final String TYPE_REWARD = "4";
    static final String TYPE_SPLASH = "7";
    static final String TYPE_FEED_LIST = "8";

    public static String transType(String type) {
        String transType;
        switch (type) {
            case ADs.TYPE_BANNER:
                transType = ADs.AD_TYPE_BANNER; //0
                break;
            case ADs.TYPE_INTERSTITIAL:
                transType = ADs.AD_TYPE_INTERSTITIAL; //1
                break;
            case ADs.TYPE_REWARD:
                transType = ADs.AD_TYPE_REWARD; //2
                break;
            case ADs.TYPE_NATIVE:
                transType = ADs.AD_TYPE_NATIVE; //3
                break;
            case ADs.TYPE_SPLASH:
                transType = ADs.AD_TYPE_SPLASH; //5
                break;
            case ADs.TYPE_FEED_LIST:
                transType = ADs.AD_TYPE_FEED_LIST; //6
                break;
            default:
                transType = ADs.AD_TYPE_UNKNOWN; //4
                break;
        }
        return transType;
    }

    public static String transSdkName(String sdkName) {
        String transSdkName;
        switch (sdkName) {
            case ADs.AD_SDK_MOPUB:
                transSdkName = "0";
                break;
            case ADs.AD_SDK_DFP:
                transSdkName = "1";
                break;
            case ADs.AD_SDK_ADMOB:
                transSdkName = "2";
                break;
            case ADs.AD_SDK_FACEBOOK:
                transSdkName = "3";
                break;
            case ADs.AD_SDK_APPLOVIN:
                transSdkName = "4";
                break;
            case AD_SDK_APPLOVIN_MAX:
                transSdkName = "5";
                break;
            case ADs.AD_SDK_UNITY:
                transSdkName = "6";
                break;
            case ADs.AD_SDK_IRON_SOURCE:
                transSdkName = "8";
                break;
            case ADs.AD_SDK_ADCOLONY:
                transSdkName = "9";
                break;
            case ADs.AD_SDK_CHARTBOOST:
                transSdkName = "10";
                break;
            case ADs.AD_SDK_VUNGLE:
                transSdkName = "11";
                break;
            case ADs.AD_SDK_DISPLAYIO:
                transSdkName = "12";
                break;
            case ADs.AD_SDK_INMOBI:
                transSdkName = "17";
                break;
            case ADs.AD_SDK_FYBER:
                transSdkName = "18";
                break;
            case ADs.AD_SDK_TOUTIAO:
                transSdkName = "19";
                break;
            case ADs.AD_SDK_GDT:
                transSdkName = "20";
                break;
            case ADs.AD_SDK_DSPMOB:
                transSdkName = "22";
                break;
            case ADs.AD_SDK_360:
                transSdkName = "24";
                break;
            case ADs.AD_SDK_4399:
                transSdkName = "25";
                break;
            case ADs.AD_SDK_BAIDU:
                transSdkName = "26";
                break;
            case ADs.AD_SDK_MOBVISTA:
                transSdkName = "27";
                break;
            case ADs.AD_SDK_OPPO:
                transSdkName = "28";
                break;
            case ADs.AD_SDK_VIVO:
                transSdkName = "29";
                break;
            case ADs.AD_SDK_XIAOMI:
                transSdkName = "30";
                break;
            case ADs.AD_SDK_CREATIVE:
                transSdkName = "31";
                break;
            case ADs.AD_SDK_DAP:
                transSdkName = "32";
                break;
            case ADs.AD_SDK_AMAZON:
                transSdkName = "33";
                break;
            case ADs.AD_SDK_FLURRY:
                transSdkName = "34";
                break;
            case ADs.AD_SDK_TAPJOY:
                transSdkName = "35";
                break;
            case ADs.AD_SDK_NEND:
                transSdkName = "36";
                break;
            case AD_SDK_MOBRAIN:
                transSdkName = "37";
                break;
            case ADs.AD_SDK_ADGENERATION:
                transSdkName = "38";
                break;
            case ADs.AD_SDK_MAIO:
                transSdkName = "39";
                break;
            case ADs.AD_SDK_ALI_GAMES:
                transSdkName = "40";
                break;
            case AD_SDK_TUIA:
                transSdkName = "42";
                break;
            case ADs.AD_SDK_CRITEO:
                transSdkName = "44";
                break;
            case ADs.AD_SDK_ZHONGHUI_ADS:
                transSdkName = "45";
                break;
            case ADs.AD_SDK_TMS:
                transSdkName = "46";
                break;
            case ADs.AD_SDK_FIVE:
                transSdkName = "47";
                break;
            case ADs.AD_SDK_KUAI_SHOU:
                transSdkName = "48";
                break;
            case AD_SDK_PANGLE:
                transSdkName = "49";
                break;
            case AD_SDK_YKY:
                transSdkName = "50";
                break;
            case AD_SDK_IMOBILE:
                transSdkName = "51";
                break;
            case AD_SDK_SIGMOB:
                transSdkName = "52";
                break;
            case AD_SDK_PREBID:
                transSdkName = "53";
                break;
            case AD_SDK_OUPENG:
                transSdkName = "54";
                break;
            case AD_SDK_APPNEXUS:
                transSdkName = "55";
                break;
            case AD_SDK_IFLY:
                transSdkName = "56";
                break;
            case AD_SDK_YOUDAO:
                transSdkName = "57";
                break;
            case AD_SDK_ALIBC:
                transSdkName = "58";
                break;
            case AD_SDK_QTT:
                transSdkName = "59";
                break;
            case AD_SDK_LINKAI:
                transSdkName = "60";
                break;
            case AD_SDK_JAD:
                transSdkName = "61";
                break;
            case AD_SDK_TopOn:
                transSdkName = "62";
                break;
            case AD_SDK_MeiShu:
                transSdkName = "64";
                break;
            case AD_SDK_TradPlus:
                transSdkName = "63";
                break;
            case AD_SDK_AppLovin_Exchange:
                transSdkName = "65";
                break;
            case AD_SDK_SAN:
                transSdkName = "66";
                break;
            case AD_SDK_VERVE:
                transSdkName = "67";
                break;
            case AD_SDK_ADFLY:
                transSdkName = "68";
                break;
            default:
                transSdkName = "99"; //UNKNOWN
                break;
        }
        return transSdkName;
    }
}